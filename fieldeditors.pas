unit fieldeditors;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Controls, StdCtrls, ExtCtrls, metadata, eventnotify,
  dbprovider, DateTimePicker, dateutils, Buttons;

type
   { TFieldEditorBase }

  TFieldEditorBase = class;

  TFieldEditorChange = procedure(Editor: TFieldEditorBase) of object;

  TFieldEditorBase = class(TCustomPanel)
    private
      FFieldInfo: TFieldInfo;
      Editor: TControl;
      id: integer;
      FTableName: string;
      DisableOnChange: boolean;
      procedure OnChange(Sender: TObject);
    public
      constructor Create(AOwner: TComponent; AFieldInfo: TFieldInfo;
        Recno: integer; ATableInfo: TTableInfo); virtual;
      procedure SetIntValue(Val: integer; Sync: boolean); virtual; abstract;
      procedure SetStrValue(Val: string; Sync: boolean); virtual; abstract;
      function GetStrValue(): string; virtual; abstract;
      function GetIntValue(): integer; virtual; abstract;
      property FieldInfo: TFieldInfo read FFieldInfo;
      destructor Destroy; override;
  end;

  TFieldEditor = class(TFieldEditorBase)
    private
      procedure OnChange(Sender: TObject);
    public
      constructor Create(AOwner: TComponent; AFieldInfo: TFieldInfo;
        Recno: integer; ATableInfo: TTableInfo); override;
      procedure SetIntValue(Val: integer; Sync: boolean); override;
      procedure SetStrValue(Val: string; Sync: boolean); override;
      function GetStrValue(): string; override;
      function GetIntValue(): integer; override;
  end;

  { TFieldForeignEditor }

  TFieldForeignEditor = class(TFieldEditorBase, IListener)
    private
      Provider: TForeignTableProvider;
      ForeignIDs: array of integer;
      procedure OnChange(Sender: TObject);
    public
      constructor Create(AOwner: TComponent; AFieldInfo: TFieldInfo;
        Recno: integer; ATableInfo: TTableInfo); override;
      procedure SetIntValue(Val: integer; Sync: boolean); override;
      procedure SetStrValue(Val: string; Sync: boolean); override;
      function GetStrValue(): string; override;
      function GetIntValue(): integer; override;
      procedure HandleMessage(Channel, Event, Details: string);
      destructor Destroy; override;
  end;

  { TFieldDateEditor }

  TFieldDateEditor = class(TFieldEditorBase)
    private
      picker: TDateTimePicker;
      procedure OnChange(Sender: TObject);
      procedure OnClick(Sender: TObject);
      procedure AddButton(APanel: TCustomPanel; ACaption: string; ATag: integer);
    public
      constructor Create(AOwner: TComponent; AFieldinfo: TFieldInfo;
        Recno: integer; ATableInfo: TTableInfo); override;
      procedure SetIntValue(Val: integer; Sync: boolean); override;
      procedure SetStrValue(Val: string; Sync: boolean); override;
      function GetStrValue(): string; override;
      function GetIntValue(): integer; override;
  end;

implementation

{ TFieldDateEditor }

procedure TFieldDateEditor.OnChange(Sender: TObject);
begin
  if DisableOnChange then exit()
  else inherited;
end;

procedure TFieldDateEditor.OnClick(Sender: TObject);
var
  c: TComponent;
begin
  c := TComponent(Sender);
  case c.Tag of
    -3: picker.Date := IncMonth(picker.Date, -12);
    -2: picker.Date := IncMonth(picker.Date, -1);
    -1: picker.Date := IncDay(picker.Date, -1);
    1: picker.Date := IncDay(picker.Date, 1);
    2: picker.Date := IncMonth(picker.Date, 1);
    3: picker.Date := IncMonth(picker.Date, 12);
  end;
  OnChange(picker);
end;

procedure TFieldDateEditor.AddButton(APanel: TCustomPanel; ACaption: string;
  ATag: integer);
var
  b: TSpeedButton;
begin
  b := TSpeedButton.Create(APanel);
  b.Tag := ATag;
  b.Caption := ACaption;
  b.OnClick := @self.OnClick;
  APanel.InsertControl(b);
end;

constructor TFieldDateEditor.Create(AOwner: TComponent;
  AFieldinfo: TFieldInfo; Recno: integer; ATableInfo: TTableInfo);
var
  p: TCustomPanel;
begin
  inherited Create(AOwner, AFieldinfo, Recno, ATableInfo);
  p := TCustomPanel.Create(self);
  p.Height := 32;
  p.Width := 296;
  p.Left := 170;
  p.Top := 8;
  p.BevelOuter := bvNone;
  p.ChildSizing.EnlargeHorizontal := crsScaleChilds;
  p.ChildSizing.HorizontalSpacing := 4;
  p.ChildSizing.LeftRightSpacing := 4;
  p.ChildSizing.Layout := cclTopToBottomThenLeftToRight;

  AddButton(p, '<Г', -3);
  AddButton(p, '<М', -2);
  AddButton(p, '<Д', -1);

  picker := TDateTimePicker.Create(self);
  picker.OnChange := @self.OnChange;
  p.InsertControl(picker);

  AddButton(p, 'Д>', 1);
  AddButton(p, 'М>', 2);
  AddButton(p, 'Г>', 3);

  self.InsertControl(p);
end;

procedure TFieldDateEditor.SetIntValue(Val: integer; Sync: boolean);
begin
  picker.Date := Val;
end;

procedure TFieldDateEditor.SetStrValue(Val: string; Sync: boolean);
begin
  picker.Date := StrToDate(Val);
end;

function TFieldDateEditor.GetStrValue: string;
begin
  exit(DateToStr(picker.Date));
end;

function TFieldDateEditor.GetIntValue: integer;
begin
  exit(-1);
end;

{ TFieldEditorBase }

procedure TFieldEditorBase.OnChange(Sender: TObject);
begin
  GetHub(FTableName + 'card' + IntToStr(id)).Send('Change', '');
end;

constructor TFieldEditorBase.Create(AOwner: TComponent; AFieldInfo: TFieldInfo;
  Recno: integer; ATableInfo: TTableInfo);
var
  l: TLabel;
begin
  inherited Create(AOwner);
  FFieldInfo := AFieldInfo;
  id := Recno;
  FTableName := ATableInfo.Name;

  self.Width := 424;
  self.Height := 36;
  self.BevelOuter := bvNone;

  l := TLabel.Create(self);
  l.AutoSize := False;
  l.Width := 148;
  l.Height := 20;
  l.Top := 11;
  l.Left := 8;
  l.Alignment := taRightJustify;
  l.Caption := AFieldInfo.Caption + ':';
  self.InsertControl(l);
end;

destructor TFieldEditorBase.Destroy;
begin
  eventnotify.GetHub(FTableName + 'card' + IntToStr(id)).Handlers.Remove(self);
  inherited Destroy;
end;

{ TFieldEditor }

procedure TFieldEditor.OnChange(Sender: TObject);
begin
  if DisableOnChange then exit()
  else inherited;
end;

constructor TFieldEditor.Create(AOwner: TComponent; AFieldInfo: TFieldInfo;
  Recno: integer; ATableInfo: TTableInfo);
var
  Ed: TEdit;
begin
  inherited Create(AOwner, AFieldInfo, Recno, ATableInfo);
  FFieldInfo := AFieldInfo;
  Ed := TEdit.Create(self);
  Ed.Width := 304;
  Ed.Height := 28;
  Ed.Left := 164;
  Ed.Top := 8;
  Editor := Ed;
  self.InsertControl(Editor);
  Ed.OnChange := @OnChange;
end;

procedure TFieldEditor.SetIntValue(Val: integer; Sync: boolean);
begin
  TEdit(Editor).Text := IntToStr(Val);
end;

procedure TFieldEditor.SetStrValue(Val: string; Sync: boolean);
begin
  TEdit(Editor).Text := Val;
end;

function TFieldEditor.GetStrValue: string;
begin
  exit(TEdit(Editor).Text);
end;

function TFieldEditor.GetIntValue: integer;
begin
  exit(StrToInt(TEdit(Editor).Text));
end;

{ TFieldForeignEditor }

procedure TFieldForeignEditor.HandleMessage(Channel, Event, Details: string);
var
  Ed: TComboBox;
  i: integer;
  v: string;
begin
  if Event = 'Update' then begin
    Ed := TComboBox(Editor);
    if Ed.ItemIndex <> -1 then
      v := Ed.Items[Ed.ItemIndex]
    else
      v := '';
    Ed.Clear;
    with Provider.Source do begin
      DataSet.First;
      SetLength(ForeignIDs, Provider.Count);
      i := 0;
      while not DataSet.EOF do begin
        Ed.Items.Add( DataSet[FFieldInfo.Name] );
        ForeignIDs[i] := DataSet['id'];
        DataSet.Next;
        inc(i);
      end;
    end;
    SetStrValue(v, False);
    Ed.ItemIndex := -1;
  end
  else if (Event = 'Sync') and (Channel = FTableName + FFieldInfo.Table.Name +
    IntToStr(id))
  then begin
    DisableOnChange := True;
    TComboBox(Editor).ItemIndex := StrToInt(Details);
    DisableOnChange := False;
  end;
end;

destructor TFieldForeignEditor.Destroy;
begin
  inherited Destroy;
end;


procedure TFieldForeignEditor.OnChange(Sender: TObject);
var
  Ed: TComboBox;
begin
  Ed := TComboBox(Sender);
  GetHub(FTableName + FFieldInfo.Table.Name + IntToStr(id)).Send('Sync',
    IntToStr(Ed.ItemIndex));
  inherited;
end;

constructor TFieldForeignEditor.Create(AOwner: TComponent;
  AFieldInfo: TFieldInfo; Recno: integer; ATableInfo: TTableInfo);
var
  Ed: TComboBox;
begin
  inherited Create(AOwner, AFieldInfo, Recno, ATableInfo);
  FFieldInfo := AFieldInfo;
  DisableOnChange := False;

  Ed := TComboBox.Create(self);
  Ed.Width := 304;
  Ed.Height := 28;
  Ed.Left := 164;
  Ed.Top := 8;
  Ed.ReadOnly := True;
  Ed.OnChange := @OnChange;
  Ed.ItemIndex := -1;

  Provider := Provide(AFieldInfo.Table.Name);

  eventnotify.GetHub(
    FTableName + FFieldInfo.Table.Name + IntToStr(id)
  ).RegisterListener(self);

  Editor := Ed;
  self.InsertControl(Editor);
  HandleMessage(AFieldInfo.Table.Name, 'Update', '');
end;

procedure TFieldForeignEditor.SetIntValue(Val: integer; Sync: boolean);
var
  i: integer;
begin
  for i := 0 to High(ForeignIDs) do begin
    if ForeignIDs[i] = Val then TComboBox(Editor).ItemIndex := i;
  end;
  if Sync then OnChange(Editor);
end;

procedure TFieldForeignEditor.SetStrValue(Val: string; Sync: boolean);
var
  Ed: TComboBox;
  x: integer;
begin
  Ed := TComboBox(Editor);
  x := Ed.Items.IndexOf(Val);
  if x = -1 then Ed.ItemIndex := 0 else Ed.ItemIndex := x;
  if Sync then OnChange(Editor);
end;

function TFieldForeignEditor.GetStrValue: string;
begin
  with TComboBox(Editor) do begin
    exit(Items[ItemIndex]);
  end;
end;

function TFieldForeignEditor.GetIntValue: integer;
begin
  Exit(ForeignIDs[TComboBox(Editor).ItemIndex]);
end;

end.

