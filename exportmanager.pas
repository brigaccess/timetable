unit exportmanager;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, metadata, Variants, ComObj, Dialogs, filters, dbprovider,
  Graphics, exceltype, LCLIntf, math, fexportprogress, ActiveX;

type

  TUpdateProgressProc = procedure (state: string; perc: integer) of object;

  { TExporter }

  TExporter = class(TObject)
    private
      FName: string;
    public
      constructor Create(AName: string);
      function ExportTimetable(Updater: TUpdateProgressProc;
        PData: PTimetableArray; SkipFields: array of boolean;
        Filters: array of TFilterPanel; Filename: string): boolean;
      virtual; abstract;
      property Name: string read FName;
  end;

  { TExcelExportThread }

  TExcelExporter = class;

  TExcelExportThread = class(TThread)
    private
        { Private declarations }
        Excel: OLEVariant;
        ProgressUpdater: TUpdateProgressProc;
        Message: string;
        Percent: integer;
        PData: PTimetableArray;
        SkipFields: array of boolean;
        Filters: array of TFilterPanel;
        Filename: string;
          const
            CWIDTH = 30;
            CHEIGHT = 30;
        function SheetRange(sheet: OLEVariant;
          row1, col1, row2, col2: integer): OLEVariant;
        function MergeCells(sheet: OLEVariant; row1, col1, row2, col2: integer
          ): OLEVariant;
        function HeaderCells(range: OLEVariant; resize: boolean): OLEVariant;
        function ColorCell(cell: OLEVariant; color: TColor): OLEVariant;
        function OutlineRange(range: OLEVariant; style, weight: integer;
          color: TColor): OLEVariant;
        procedure UpdateProgress_;
        procedure UpdateProgress(m: string; p: integer);
    protected
        procedure Execute; override;
    public
        procedure Init(Updater: TUpdateProgressProc;
          AData: PTimetableArray;
          ASkipFields: array of boolean; AFilters: array of TFilterPanel;
          AFilename: string);
  end;

  { TExcelExporter }

  TExcelExporter = class(TExporter)
      function ExportTimetable(Updater: TUpdateProgressProc;
        PData: PTimetableArray; SkipFields: array of boolean;
        Filters: array of TFilterPanel; Filename: string): boolean; override;
  end;

  var
    Exporters: array of TExporter;

  procedure ExportTable(Parent: TComponent; Exporter: integer; PData: PTimetableArray;
        SkipFields: array of boolean; Filters: array of TFilterPanel;
        Filename: string);
  procedure AddExporter(AExporter: TExporter);

implementation

procedure ExportTable(Parent: TComponent; Exporter: integer; PData: PTimetableArray;
  SkipFields: array of boolean; Filters: array of TFilterPanel; Filename: string
  );
var
  exp: TExporter;
  statewin: TFormExportProgress;
begin
  exp := Exporters[Exporter];
  statewin := TFormExportProgress.Create(Parent);
  try
    exp.ExportTimetable(@statewin.UpdateProgress, PData, SkipFields, Filters,
       Filename);
    statewin.ShowModal;
  except on E: Exception do begin
    MessageDlg('Ошибка экспорта',
      Format('При экспорте в формат %s произошла ошибка: '+ #13#10 + '%s',
        [exp.Name, e.Message]), mtError, [mbOK], ''
    );
    statewin.Release;
    end;
  end;
end;

procedure AddExporter(AExporter: TExporter);
begin
  SetLength(Exporters, Length(Exporters) + 1);
  Exporters[High(Exporters)] := AExporter;
end;

{ TExcelExportThread }

procedure TExcelExportThread.Execute;
var
  workbook, sheet, range: OLEVariant;
  s, tmp: widestring;
  i, headeroffset, startoffset: integer;
  row, col, k, maxk, ii, len: integer;
begin
  try
    CoInitialize(Nil);
    Excel := CreateOleObject('Excel.Application');
    Excel.Visible := False;
    Excel.DisplayAlerts := True;
  except on e: Exception do begin
    Writeln(UTF8Decode(e.Message));
    MessageDlg('Ошибка Excel',
      'Невозможно создать OLE-объект Excel: ' + #13#10 + e.Message,
    mtError, [mbOK], '');
    UpdateProgress('FINISH', 0);
    exit();
  end
  end;
  try
    UpdateProgress('Создание книги...', 0);
    workbook := Excel.Workbooks.Add;
    UpdateProgress('Создание листа...', 1);
    sheet := workbook.Worksheets.Add;
    sheet.Name := ExcStr('Расписание');
    s := ExcStr(Filename);
    UpdateProgress('Предварительное сохранение файла...', 2);
    workbook.SaveAs(s);
    Excel.DisplayAlerts := False;

    headeroffset := 4;

    sheet.Cells[1, 1].Formula := ExcStr('Время составления отчета: ');
    sheet.Cells[1, 2].Formula := ExcStr(DateTimeToStr(Now));

    if Length(Filters) > 0 then begin
      sheet.Cells[headeroffset, 1].Formula :=
        ExcStr('Использованные фильтры:');
      for i := 0 to High(Filters) do begin
        if (Filters[i].Op = '') or (Filters[i].FieldID = -1) then continue;
        sheet.Cells[headeroffset, 2].Formula :=
          ExcStr(Filters[i].ComboFields.Items[Filters[i].ComboFields.ItemIndex]);
        sheet.Cells[headeroffset, 3].Formula := ExcStr(Filters[i].Op);
        sheet.Cells[headeroffset, 4].Formula := ExcStr(Filters[i].Parameter);
        headeroffset += 1;
        UpdateProgress('Экспорт фильтров...', 3);
      end;
    end;
    workbook.Save();
    headeroffset += 1;

    HeaderCells(
      SheetRange(sheet, headeroffset, 1, headeroffset, Length(PData^)), true);

    for i := 1 to High(PData^) do begin
      sheet.Cells[headeroffset, i + 1].Formula := ExcStr(PData^[i, 0, 0].info[0]);
    end;
    workbook.Save();

    headeroffset += 1;
    startoffset := headeroffset;
    len := Length(PData^[0]) * Length(PData^);
    for row := 1 to High(PData^[0]) do begin
      maxk := 0;
      for col := 1 to High(PData^) do begin
        for k := 0 to High(PData^[col, row]) do begin
          tmp := '';
          if k > maxk then maxk := k;

          for ii := 0 to High(PData^[col, row, k].info) do begin
            if Length(tmp) > 0 then tmp += #13#10;
            tmp += PData^[col, row, k].info[ii];
          end;

          range := sheet.Cells[headeroffset + k, col + 1];
          range.Formula := ExcStr(tmp);
          if Length(PData^[col, row, k].conflicts) > 0 then
            ColorCell(range, RGBToColor(255, 217, 102));
        end;
        UpdateProgress(Format('Экспорт записей... %d/%d ячеек',
          [(row * Length(PData^) + col), len]),
          5 + Ceil(95 * ((row * Length(PData^) + col) / len)));
      end;
      range := MergeCells(sheet, headeroffset, 1, headeroffset + maxk, 1);
      range.Formula := ExcStr(PData^[0, row, 0].info[0]);
      range := OutlineRange(
        SheetRange(sheet, headeroffset, 1, headeroffset + maxk, Length(PData^)),
        xlContinuous, xlThick, clBlack
      );
      range.Borders[xlInsideHorizontal].Weight := xlThin;
      range.Borders[xlInsideVertical].Weight := xlMedium;
      workbook.Save();
      headeroffset += maxk + 1;
    end;

    HeaderCells(SheetRange(sheet, startoffset, 1, headeroffset - 1, 1), False);
    workbook.Save();
  except on e: Exception do begin
    MessageDlg('Ошибка Excel',
      'Ошибка при экспорте: ' + #13#10 + ExcStr(e.Message),
    mtError, [mbOK], '');
    UpdateProgress('FINISH', 0);
    exit();
  end;
  end;
  Excel.DisplayAlerts := False;  // Suppress "Do you want to save?" dialog
  Excel.Quit;
  Excel := Unassigned;
  UpdateProgress('FINISH', 0);
end;

procedure TExcelExportThread.Init(Updater: TUpdateProgressProc;
  AData: PTimetableArray; ASkipFields: array of boolean;
  AFilters: array of TFilterPanel; AFilename: string);
var
  i: integer;
begin
  ProgressUpdater := Updater;
  PData := AData;
  SetLength(SkipFields, Length(ASkipFields));
  for i := 0 to High(ASkipFields) do begin
    SkipFields[i] := ASkipFields[i];
  end;
  SetLength(Filters, Length(AFilters));
  for i := 0 to High(AFilters) do begin
    Filters[i] := AFilters[i];
  end;
  Filename := AFilename;
end;

{ TExcelExporter }

function TExcelExportThread.SheetRange(sheet: OLEVariant; row1, col1, row2, col2: integer
  ): OLEVariant;
begin
  exit(sheet.Range[sheet.Cells[row1, col1], sheet.Cells[row2, col2]]);
end;

function TExcelExportThread.MergeCells(sheet: OLEVariant; row1, col1, row2,
  col2: integer): OLEVariant;
begin
  Result := SheetRange(sheet, row1, col1, row2, col2);
  Result.Merge;
  Result := sheet.Cells[row1, col1];
  Result.HorizontalAlignment := xlCenter;
  Result.VerticalAlignment := xlCenter;
  Result.Orientation := 0;
end;

function TExcelExportThread.HeaderCells(range: OLEVariant; resize: boolean
  ): OLEVariant;
begin
  range.WrapText := True;
  range.HorizontalAlignment := xlLeft;
  range.VerticalAlignment := xlCenter;
  if resize then begin
    if range.ColumnWidth <> CWIDTH then
      range.ColumnWidth := CWIDTH;
    if range.RowHeight <> CHEIGHT then
      range.RowHeight := CHEIGHT;
  end;
  ColorCell(range, RGBToColor(221, 235, 247));
  exit(range);
end;

function TExcelExportThread.ColorCell(cell: OLEVariant; color: TColor): OLEVariant;
begin
  cell.Interior.Pattern := xlSolid;
  cell.Interior.Color := color;
  exit(cell);
end;

function TExcelExportThread.OutlineRange(range: OLEVariant; style, weight: integer;
  color: TColor): OLEVariant;
begin
  range.BorderAround(style, weight, color);
  exit(range);
end;

procedure TExcelExportThread.UpdateProgress_;
begin
  ProgressUpdater(Message, Percent);
end;

procedure TExcelExportThread.UpdateProgress(m: string; p: integer);
begin
  Message := m;
  Percent := p;
  Synchronize(@UpdateProgress_);
end;

function TExcelExporter.ExportTimetable(Updater: TUpdateProgressProc;
  PData: PTimetableArray; SkipFields: array of boolean;
  Filters: array of TFilterPanel; Filename: string): boolean;
var
  thread: TExcelExportThread;
begin
  try
    thread := TExcelExportThread.Create(True);
    thread.Init(Updater, PData, [], Filters, Filename);
    thread.FreeOnTerminate := True;
    thread.Start;
  except

  end;
end;

{ TExporter }

constructor TExporter.Create(AName: string);
begin
  FName := AName;
end;

initialization
  AddExporter(TExcelExporter.Create('Excel'));

end.

