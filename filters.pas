unit filters;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Controls, StdCtrls, ExtCtrls, Buttons, metadata, contnrs;

type
  TFilterPanel = class;

  TFilterAddHook = procedure() of object;
  TFilterRemoveHook = procedure(Filter: TFilterPanel) of object;
  TFilterChangeHook = procedure(Filter: TFilterPanel) of object;

  { TFilterPanel }

  TFilterPanel = class(TPanel)
  private
    FTable: TTableInfo;
    FOwner: TComponent;

    FAddHook: TFilterAddHook;
    FRemoveHook: TFilterRemoveHook;
    FChangeHook: TFilterChangeHook;
    function GetFieldID(): integer;
    function GetOp(): string;
    function GetParameter(): string;
  public
    ComboFields, ComboOps: TComboBox;
    EditValue: TEdit;
    SpeedAddFilter, SpeedDeleteFilter: TSpeedButton;
    constructor Create(TheOwner: TComponent; Table: TTableInfo;
      AddHook: TFilterAddHook; RemoveHook: TFilterRemoveHook;
      ChangeHook: TFilterChangeHook);
    procedure OnComboFieldsChange(Sender: TObject);
    procedure OnComboOpsChange(Sender: TObject);
    procedure OnEditValueChange(Sender: TObject);
    procedure OnSpeedAddFilterClick(Sender: TObject);
    procedure OnSpeedDeleteFilterClick(Sender: TObject);

    property FieldID: integer read GetFieldID;
    property Op: string read GetOp;
    property Parameter: string read GetParameter;
  end;

  function GetFilterQuery(Table: TTableInfo; Filters: TFPObjectList): string;

implementation

function GetFilterQuery(Table: TTableInfo; Filters: TFPObjectList): string;
var
  i, j: integer;
  f: TFilterPanel;
  finfo: TFieldInfo;
  query, curquery, trimpar: string;
  pars: array of record
    value: String;
    field: TFieldInfo;
    filter: integer;
  end;
begin
    if Filters.Count > 0 then begin
      query := ' WHERE ';

      for i := 0 to Filters.Count - 1 do begin
        f := TFilterPanel(Filters.Items[i]);
        // Getting rid of invalid fields
        if (f.FieldID = -1) or (f.Op = '') then
          continue;

        if (Length(pars) > 0) then curquery := ' AND ' else curquery := '';

        with Table do begin
          finfo := DisplayFields[f.FieldID];
          // Table lookup for foreign keys
          if finfo.Table <> Table then begin
            for j := 0 to High(Relations) do begin
              if finfo.Table.Name = Relations[j].Joined.Name then begin
                curquery += 't' + IntToStr(j + 1);
                break;
              end;
            end;
          end
          else
            curquery += 't0';
        end;

        curquery += '.' + finfo.Name + ' ';

        trimpar := Trim(f.Parameter);
        if (trimpar = '') then
          curquery += ' IS NULL '
        else begin
          curquery += f.Op + ' :prm' + IntToStr(Length(pars));
          SetLength(pars, Length(pars) + 1);
          pars[High(pars)].filter := i;
          pars[High(pars)].value := trimpar;
          pars[High(pars)].field := finfo;
        end;
        query += curquery;
      end;
      if query <> ' WHERE ' then begin
        exit(query);
      end
    end;
    exit('');
end;

{ TFilterPanel }

function TFilterPanel.GetFieldID: integer;
begin
  if ComboFields.Items.Count = 0 then exit(-1);
  Result := ComboFields.ItemIndex;
end;

function TFilterPanel.GetOp: string;
begin
  if ComboOps.ItemIndex = -1 then
    Exit('');
  Result := ComboOps.Items.Strings[ComboOps.ItemIndex];
end;

function TFilterPanel.GetParameter: string;
begin
  Result := EditValue.Text;
end;

constructor TFilterPanel.Create(TheOwner: TComponent; Table: TTableInfo;
  AddHook: TFilterAddHook; RemoveHook: TFilterRemoveHook;
  ChangeHook: TFilterChangeHook);
var
  f: TFieldInfo;
  First: boolean;
begin
  inherited Create(TheOwner);
  FTable := Table;
  FOwner := TheOwner;
  FAddHook := AddHook;
  FRemoveHook := RemoveHook;
  FChangeHook := ChangeHook;

  self.Width := 470;
  self.Height := 34;
  self.BorderStyle := bsNone;
  self.BevelInner := bvNone;
  self.BevelOuter := bvNone;
  self.ChildSizing.Layout := cclLeftToRightThenTopToBottom;
  self.ChildSizing.HorizontalSpacing := 3;
  self.ChildSizing.LeftRightSpacing := 3;
  self.ChildSizing.TopBottomSpacing := 3;
  self.ChildSizing.ControlsPerLine := 6;

  ComboFields := TComboBox.Create(self);

  First := True;

  for f in Table.DisplayFields do begin
    if First then begin
      First := False;
    end;
    ComboFields.Items.Add(f.Caption);
  end;

  ComboFields.ReadOnly := True;
  ComboFields.OnChange := @OnComboFieldsChange;
  self.InsertControl(ComboFields);

  ComboOps := TComboBox.Create(self);
  ComboOps.Constraints.MinWidth := 150;
  ComboOps.ReadOnly := True;
  ComboOps.OnChange := @OnComboOpsChange;
  self.InsertControl(ComboOps);

  EditValue := TEdit.Create(self);
  EditValue.AutoSize := False;
  EditValue.Constraints.MinWidth := 200;
  EditValue.OnChange := @OnEditValueChange;
  self.InsertControl(EditValue);

  SpeedDeleteFilter := TSpeedButton.Create(self);
  SpeedDeleteFilter.Caption := '-';
  SpeedDeleteFilter.Constraints.MinWidth := 24;
  SpeedDeleteFilter.OnClick := @OnSpeedDeleteFilterClick;
  self.InsertControl(SpeedDeleteFilter);

  SpeedAddFilter := TSpeedButton.Create(self);
  SpeedAddFilter.Caption := '+';
  SpeedAddFilter.Constraints.MinWidth := 24;
  SpeedAddFilter.OnClick := @OnSpeedAddFilterClick;
  self.InsertControl(SpeedAddFilter);
end;

procedure TFilterPanel.OnComboFieldsChange(Sender: TObject);
var
  i: integer;
begin
  ComboOps.Items.Clear;
  if FTable.ActualFields[(Sender as TComboBox).ItemIndex].FieldType = Numerical then begin
    for i := 0 to High(NumericalOps) do begin
      ComboOps.Items.Add(NumericalOps[i]);
    end;
  end
  else begin
    for i := 0 to High(VarcharOps) do begin
      ComboOps.Items.Add(VarcharOps[i]);
    end;
  end;
  ComboOps.ItemIndex := -1;
  FChangeHook(self);
end;

procedure TFilterPanel.OnComboOpsChange(Sender: TObject);
begin
  FChangeHook(self);
end;

procedure TFilterPanel.OnEditValueChange(Sender: TObject);
begin
  FChangeHook(self);
end;

procedure TFilterPanel.OnSpeedAddFilterClick(Sender: TObject);
begin
  FAddHook();
  (Sender as TControl).Visible := False;
end;

procedure TFilterPanel.OnSpeedDeleteFilterClick(Sender: TObject);
begin
  inherited;
  FRemoveHook(self);
end;

end.

