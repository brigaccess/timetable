unit fView;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, IBConnection, sqldb, DB, FileUtil, Forms, Controls,
  Graphics, Dialogs, Menus, DBGrids, ExtCtrls, Buttons, DbCtrls,
  metadata, dbprovider, contnrs, fCard, eventnotify, LCLType, filters;

type

  { TViewForm }

  TViewForm = class(TForm, IListener)
    DataSource: TDataSource;
    DBGrid: TDBGrid;
    MainMenu: TMainMenu;
    MenuItemClose: TMenuItem;
    MenuItemFile: TMenuItem;
    BottomPanel: TPanel;
    ScrollBoxFilters: TScrollBox;
    SpeedButtonApply: TSpeedButton;
    SpeedButtonAddFirst: TSpeedButton;
    SpeedButtonNew: TSpeedButton;
    SpeedButtonDelete: TSpeedButton;
    Splitter1: TSplitter;
    SQLQuery: TSQLQuery;
    SQLTransaction: TSQLTransaction;
    procedure DBGridCellClick(Column: TColumn);
    procedure DBGridDblClick(Sender: TObject);
    procedure DBGridKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure MenuItemCloseClick(Sender: TObject);
    procedure AddFilter(); overload; // Awful legacy
    function AddNewFilter(): TFilterPanel; overload;
    procedure AddFilter(AField, Op, Value: String); overload;
    procedure RemoveFilter(Filter: TFilterPanel);
    procedure ChangeFilter(Filter: TFilterPanel);
    procedure SpeedButtonAddFirstClick(Sender: TObject);
    procedure SpeedButtonApplyClick(Sender: TObject);
    procedure PrettifyTable();
    procedure SpeedButtonDeleteClick(Sender: TObject);
    procedure SpeedButtonNewClick(Sender: TObject);
    procedure HandleMessage(Channel, Event, Details: String);
  public
    constructor Create(TheOwner: TComponent; ATableId: integer);
  private
    { private declarations }
    FTable: TTableInfo;
    Filters: TFPObjectList;
  public
    { public declarations }
  end;

implementation

{$R *.lfm}

{ TViewForm }

procedure TViewForm.MenuItemCloseClick(Sender: TObject);
begin
  self.Close;
end;

procedure TViewForm.AddFilter;
begin
  AddNewFilter();
end;

procedure TViewForm.DBGridDblClick(Sender: TObject);
var
  id: integer;
begin
  id := DBGrid.DataSource.DataSet.FieldByName('id').CurValue;
  fCard.EditItem(self, FTable.Name, id);
end;

procedure TViewForm.DBGridCellClick(Column: TColumn);
begin
  SpeedButtonDelete.Enabled := True;
end;

procedure TViewForm.DBGridKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_DELETE) and SpeedButtonDelete.Enabled then
    SpeedButtonDelete.Click
  else if (Key = VK_RETURN) then
    DBGridDblClick(Sender)
end;

function TViewForm.AddNewFilter: TFilterPanel;
var
  fp: TFilterPanel;
begin
  ScrollBoxFilters.Visible := True;
  SpeedButtonApply.Enabled := True;
  fp := TFilterPanel.Create(self, FTable, @self.AddFilter, @self.RemoveFilter,
    @self.ChangeFilter);
  Filters.Add(fp);
  ScrollBoxFilters.InsertControl(fp);
  exit(fp);
end;

procedure TViewForm.AddFilter(AField, Op, Value: String);
var
  fp: TFilterPanel;
begin
  fp := AddNewFilter();
  fp.ComboFields.ItemIndex := fp.ComboFields.Items.IndexOf(AField);
  fp.OnComboFieldsChange(fp.ComboFields);
  fp.ComboOps.ItemIndex := fp.ComboOps.Items.IndexOf(Op);
  fp.EditValue.Text := Value;
end;

procedure TViewForm.RemoveFilter(Filter: TFilterPanel);
begin
  ScrollBoxFilters.RemoveControl(Filter);
  Filters.Delete(Filters.IndexOf(Filter));
  if (Filters.Count > 0) then begin
    (Filters.Last as TFilterPanel).SpeedAddFilter.Visible := True;
  end
  else begin
    ScrollBoxFilters.Visible := False;
  end;
  SpeedButtonApply.Enabled := True;
end;

procedure TViewForm.ChangeFilter(Filter: TFilterPanel);
begin
  SpeedButtonApply.Enabled := True;
end;

procedure TViewForm.SpeedButtonAddFirstClick(Sender: TObject);
begin
  AddFilter();
end;

procedure TViewForm.SpeedButtonApplyClick(Sender: TObject);
var
  i: integer;
  x: string;
  p: TFilterPanel;
begin
  x := FTable.GetSelectQuery() + GetFilterQuery(FTable, Filters);
  SQLTransaction.Commit;
  SQLQuery.Close;
  SQLQuery.SQL.Text := x;
  SQLQuery.Prepare;
  for i := 0 to Filters.Count - 1 do begin
    p := TFilterPanel(Filters.Items[i]);
    if (p.FieldID = -1) or (p.Op = '') or (p.Parameter = '') then continue;
    try
      SQLQuery.ParamByName('prm' + IntToStr(i)).AsString := p.EditValue.Text;
    except
      ShowMessage(Format('Ошибка в фильтре "%s %s %s". Запрос отменен.',
        [
          FTable.DisplayFields[p.FieldID].Caption,
          p.Op,
          p.EditValue.Text
        ]));
    end;
  end;
  SQLQuery.Open;
  PrettifyTable();
end;

procedure TViewForm.PrettifyTable;
var
  i, ci: integer;
  c: TColumn;
begin
  for i := 0 to DBGrid.Columns.Count - 1 do begin
      c := DBGrid.Columns.Items[i];
      ci := FTable.GetConcatInfoId(FTable.DisplayFields[i]);
      if ci <> -1 then
        c.Title.Caption := FTable.DisplayFields[i].Table.RecordName
      else
        c.Title.Caption := FTable.DisplayFields[i].Caption;
      c.Width := FTable.DisplayFields[i].Width;
  end;
end;

procedure TViewForm.SpeedButtonDeleteClick(Sender: TObject);
var
  id: integer;
  ans: integer;
  editor: TEditForm;
begin
  id := DBGrid.DataSource.DataSet.FieldByName('id').CurValue;
  ans := Application.MessageBox(
      'Вы действительно хотите удалить эту запись?',
      'Удаление',
      MB_YESNO + MB_ICONQUESTION
    );
  if (ans = IDYES) then  begin
    editor := fCard.GetCard(FTable.Name, id);
    if editor <> Nil then editor.ForceClose();
    try
      dbprovider.Provide(FTable.Name).DeleteItem(id);
      ShowMessage('Запись удалена');
    except on e: Exception do
    begin
      ShowMessage('Ошибка при удалении записи: ' + #13#10 + e.Message);
    end;
    end;
  end;
end;

procedure TViewForm.SpeedButtonNewClick(Sender: TObject);
begin
  FCard.EditItem(self, FTable.Name, -1);
end;

constructor TViewForm.Create(TheOwner: TComponent; ATableId: integer);
begin
  inherited Create(TheOwner);
  FTable := metadata.RegisteredTables[ATableId];
  Filters := TFPObjectList.Create;
  self.Caption := self.Caption + FTable.Caption;
  SQLTransaction.DataBase := DbData.DBConnection;
  SQLQuery.DataBase := DbData.DBConnection;
  SQLQuery.Transaction := SQLTransaction;
  try
    SQLQuery.Close;
    SQLQuery.SQL.Text := FTable.GetSelectQuery();
    SQLQuery.Open;
    PrettifyTable;
    eventnotify.GetHub(FTable.Name).RegisterListener(self);
  except
    on E: Exception do begin
      ShowMessage('Error: ' + E.Message);
      self.Close;
    end;
  end;
end;

procedure TViewForm.HandleMessage(Channel, Event, Details: String);
var
  item: integer;
begin
  if (Event = 'Update') then begin
    item := DBGrid.DataSource.DataSet.RecNo;
    SQLTransaction.Commit;
    SQLQuery.Close;
    SQLQuery.Open;
    PrettifyTable;
    with DBGrid.DataSource.DataSet do begin
      if item > RecordCount then
        RecNo := RecordCount
      else
        RecNo := item;
    end;
  end;
end;

end.
