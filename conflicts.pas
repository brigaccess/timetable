unit conflicts;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, sqldb, db, dbprovider, metadata, eventnotify;

type

  { TConflictType }

  TConflictType = class(TConflictTypeAbstract)
    private
      FName, FDescription: string;
    public
      constructor Create(AName, ADescription: string); override;
      function GetConflictRecords(rowname, colname: string;
        left, right: TDate): TConflictRecords; virtual; abstract;
      property Name: string read FName;
      property Description: string read FDescription;
  end;

  { TMultipleValuesConflict }

  TMultipleValuesConflict = class(TConflictType)
    private
      FSelectTable, FHavingTable: TTableInfo;
      FSelectLeft, FSelectRight, FHavingLeft, FHavingRight: string;
      function GetConflictQuery(rowname, colname: string;
        left, right: TDate): string;
    public
      constructor Create(AName, ADescription, ASelect, AHaving: string);
      function GetConflictRecords(rowname, colname: string;
        left, right: TDate): TConflictRecords; override;
  end;

  { TSmallClassroomConflict }

  TSmallClassroomConflict = class(TConflictType)
    private
      function GetConflictQuery(rowname, colname: string;
        left, right: TDate): string;
    public
      function GetConflictRecords(rowname, colname: string;
        left, right: TDate): TConflictRecords; override;
  end;

  TConflictHandler = procedure (t: TConflictType) of object;

  { TConflictManager }

  TConflictManager = class(TDataModule)
    DataSource: TDataSource;
    SQLQuery: TSQLQuery;
    SQLTransaction: TSQLTransaction;
  private
    { private declarations }
  public
    constructor Create(AOwner: TComponent); override;
    procedure GetConflicts(Handler: TConflictHandler);
  end;

var
  ConflictManager: TConflictManager;
  ConflictTypes: array of TConflictType;
  const Q_WEEK: string = 'weekday_id';
  const Q_TIME: string = 'lesson_time_id';
  procedure AddConflict(AConflict: TConflictType);

implementation

procedure AddConflict(AConflict: TConflictType);
begin
  SetLength(ConflictTypes, Length(ConflictTypes) + 1);
  ConflictTypes[High(ConflictTypes)] := AConflict;
end;

{$R *.lfm}

{ TConflictType }

constructor TConflictType.Create(AName, ADescription: string);
begin
  FName := AName;
  FDescription := ADescription;
end;

{ TSmallClassroomConflict }

function TSmallClassroomConflict.GetConflictQuery(rowname, colname: string;
  left, right: TDate): string;
var
  ttstr: string;
begin
  ttstr := Format('FullTimetable(''%s'', ''%s'') ',
        [DateToStr(left), DateToStr(right)]);
  Result := Format('SELECT t0.ID, t0.%s, t0.%s, t0.rdate, t0.%s, t0.%s, t0.%s, ',
    [rowname, colname, Q_WEEK, Q_TIME, 'group_id', 'classroom_id']);
  Result += 't1.NAME, t1.STUDENTS_COUNT, tx.NAME, tx.CAPACITY, tx.CNT, ';
  Result += 't2.name, t3.begin_ || '' - '' || t3.end_ FROM ';
  Result += ttstr + ' t0 ';
  Result += 'LEFT JOIN GROUPS t1 ON t0.GROUP_ID = t1.ID ';
  Result += 'LEFT JOIN weekdays t2 ON t0.weekday_id = t2.id ';
  Result += 'LEFT JOIN lesson_times t3 ON t0.lesson_time_id = t3.id ';
  Result += 'INNER JOIN (';
  Result += 'SELECT tx2.ID, tx2.NAME, tx2.CAPACITY, ' +
    'SUM(tx1.STUDENTS_COUNT) as CNT, tx0.rdate, tx0.WEEKDAY_ID, tx0.LESSON_TIME_ID ' +
    'FROM ' + ttstr + 'tx0 LEFT JOIN GROUPS tx1 on tx0.GROUP_ID = tx1.ID ' +
    'LEFT JOIN CLASSROOMS tx2 on tx0.CLASSROOM_ID = tx2.ID ' +
    'GROUP BY tx2.ID, tx2.NAME, tx2.CAPACITY, tx0.rdate, tx0.WEEKDAY_ID, tx0.LESSON_TIME_ID ' +
    'HAVING SUM(tx1.STUDENTS_COUNT) > tx2.CAPACITY ' +
    'ORDER BY tx0.rdate, tx0.WEEKDAY_ID, tx0.LESSON_TIME_ID) tx ' +
    'ON t0.CLASSROOM_ID = tx.id AND t0.rdate = tx.rdate AND t0.WEEKDAY_ID = tx.WEEKDAY_ID ' +
    'AND t0.LESSON_TIME_ID = tx.LESSON_TIME_ID ORDER BY 2, 3, 4, 5, 6';
end;

function TSmallClassroomConflict.GetConflictRecords(rowname, colname: string;
  left, right: TDate): TConflictRecords;
begin
  with ConflictManager do begin
    SQLTransaction.Commit;
    SQLQuery.Close;
    SQLQuery.SQL.Text := GetConflictQuery(rowname, colname, left, right);
    SQLQuery.Open;
    while not DataSource.DataSet.EOF do begin
      SetLength(Result, Length(Result) + 1);
      with Result[High(Result)] do begin
        id := DataSource.DataSet.Fields[0].AsInteger;
        row := DataSource.DataSet.Fields[1].AsInteger;
        col := DataSource.DataSet.Fields[2].AsInteger;
        date := DataSource.DataSet.Fields[3].AsDateTime;
        category := Format('%s (%d/%d) - %s, %s', [
            DataSource.DataSet.Fields[9].AsString,
            DataSource.DataSet.Fields[11].AsInteger,
            DataSource.DataSet.Fields[10].AsInteger,
            DataSource.DataSet.Fields[12].AsString,
            DataSource.DataSet.Fields[13].AsString
          ]);
        details := Format('%s (%d)', [
            DataSource.DataSet.Fields[7].AsString,
            DataSource.DataSet.Fields[8].AsInteger
        ]);
      end;
      DataSource.DataSet.Next;
    end;
  end;
end;

{ TMultipleValuesConflict }

function TMultipleValuesConflict.GetConflictQuery(rowname, colname: string;
  left, right: TDate): string;
var
  i: integer;
  tt: TTableInfo;
  ttstr: string;
begin
  tt := GetTableInfo(TIMETABLENAME);
  ttstr := Format('FullTimetable(''%s'', ''%s'') ',
        [DateToStr(left), DateToStr(right)]);

  Result := Format(
    'SELECT t0.id, t0.%s, t0.%s, t0.rdate, t0.weekday_id, t0.lesson_time_id, ',
    [rowname, colname]
  );

  for i := 1 to High(FSelectTable.DisplayFields) do begin
    if i > 1 then Result += ' || '' '' || ';
    Result += Format('ts.%s', [FSelectTable.DisplayFields[i].Name]);
  end;
  Result += ', ';
  for i := 1 to High(FHavingTable.DisplayFields) do begin
    if i > 1 then Result += ' || '' '' || ';
    Result += Format('th.%s', [FHavingTable.DisplayFields[i].Name]);
  end;
  Result += ', twd.name, tlt.begin_ || '' - '' || tlt.end_ ';
  Result += Format('FROM %s t0 ', [ttstr]);
  Result += Format('LEFT JOIN %s ts ON t0.%s = ts.%s ',
         [FSelectTable.Name, FSelectLeft, FSelectRight]);
  Result += Format('LEFT JOIN %s th ON t0.%s = th.%s ',
         [FHavingTable.Name, FHavingLeft, FHavingRight]);
  Result += 'LEFT JOIN weekdays twd ON t0.weekday_id = twd.id ';
  Result += 'LEFT JOIN lesson_times tlt ON t0.lesson_time_id = tlt.id ';
  Result += Format('INNER JOIN (SELECT tx0.%s, ', [FSelectLeft]);
  Result += Format('tx0.rdate, tx0.weekday_id, tx0.lesson_time_id FROM %s tx0 ',
         [ttstr]);
  Result += Format('GROUP BY tx0.%s, tx0.rdate, tx0.weekday_id, tx0.lesson_time_id ',
         [FSelectLeft]);
  Result += Format('HAVING COUNT(DISTINCT tx0.%s) > 1 ', [FHavingLeft]);
  Result += 'ORDER BY tx0.rdate, tx0.weekday_id, tx0.lesson_time_id) tx ';
  Result += Format('ON t0.%s = tx.%s ', [FSelectLeft, FSelectLeft]);
  Result += 'AND t0.rdate = tx.rdate ';
  Result += 'AND t0.weekday_id = tx.weekday_id ';
  Result += 'AND t0.lesson_time_id = tx.lesson_time_id';
end;

constructor TMultipleValuesConflict.Create(AName, ADescription, ASelect,
  AHaving: string);
var
  rel: integer;
  tt: TTableInfo;
begin
  inherited Create(AName, ADescription);
  FSelectTable := GetTableInfo(ASelect);
  FHavingTable := GetTableInfo(AHaving);
  tt := GetTableInfo(TIMETABLENAME);
  rel := tt.GetRelationId(FSelectTable.Name);
  if rel = -1 then
    raise Exception.Create('Can''t find relation for SELECT table of ' +
          self.Name);
  FSelectLeft := tt.Relations[rel].Left;
  FSelectRight := tt.Relations[rel].Right;
  rel := tt.GetRelationId(FHavingTable.Name);
  if rel = -1 then
    raise Exception.Create('Can''t find relation for HAVING table of ' +
          self.Name);
  FHavingLeft := tt.Relations[rel].Left;
  FHavingRight := tt.Relations[rel].Right;

end;

function TMultipleValuesConflict.GetConflictRecords(rowname, colname: string;
  left, right: TDate): TConflictRecords;
begin
  try
    with ConflictManager do begin
      SQLTransaction.Commit;
      SQLQuery.Close;
      SQLQuery.SQL.Text := GetConflictQuery(rowname, colname, left, right) +
      ' ORDER BY 2, 3, 4, 5, 6';
      SQLQuery.Open;
      DataSource.DataSet.First;
      while not DataSource.DataSet.EOF do begin
        SetLength(Result, Length(Result) + 1);
        with Result[High(Result)] do begin
          id := DataSource.DataSet.Fields[0].AsInteger;
          row := DataSource.DataSet.Fields[1].AsInteger;
          col := DataSource.DataSet.Fields[2].AsInteger;
          date := DataSource.DataSet.Fields[3].AsDateTime;
          category := Format('%s - %s, %s', [
            DataSource.DataSet.Fields[6].AsString,
            DataSource.DataSet.Fields[8].AsString,
            DataSource.DataSet.Fields[9].AsString
          ]);
          details := Format('ID %d - %s',
                  [id, DataSource.DataSet.Fields[7].AsString]);
        end;
        ConflictManager.DataSource.DataSet.Next;
      end;
    end;
  except on e: Exception do begin
    raise e;
  end;
  end;
end;

{ TConflictManager }

constructor TConflictManager.Create(AOwner: TComponent);
var
  i: integer;
begin
  inherited Create(AOwner);
  SQLTransaction.DataBase := dbprovider.DbData.DBConnection;
  SQLQuery.Transaction := SQLTransaction;
  DataSource.DataSet := SQLQuery;
end;

procedure TConflictManager.GetConflicts(Handler: TConflictHandler);
var
  i: integer;
begin
  for i := 0 to High(ConflictTypes) do begin
    Handler(ConflictTypes[i]);
  end;
end;

initialization
  AddConflict(TSmallClassroomConflict.Create('Classroom_TooSmall',
    'Кабинет слишком маленький'));

  sel := 'Groups';
  AddConflict(TMultipleValuesConflict.Create('Group_MultClassrooms',
    'Группа в нескольких кабинетах одновременно',
    sel, 'Classrooms'
  ));

  AddConflict(TMultipleValuesConflict.Create('Group_MultLessons',
    'Группа на нескольких разных занятиях одновременно',
    sel, 'Lessons'
  ));

  AddConflict(TMultipleValuesConflict.Create('Group_MultForms',
    'Группа на нескольких видах занятия одновременно',
    sel, 'Lesson_Types'
  ));

  AddConflict(TMultipleValuesConflict.Create('Group_MultTeachers',
    'Группа занимается с несколькими преподавателями одновременно',
    sel, 'Teachers'
  ));

  sel := 'Teachers';
  AddConflict(TMultipleValuesConflict.Create('Teacher_MultClassrooms',
    'Преподаватель в нескольких кабинетах одновременно',
    sel, 'Classrooms'
  ));

  AddConflict(TMultipleValuesConflict.Create('Teacher_MultLessons',
    'Преподаватель ведет несколько разных дисциплин одновременно',
    sel, 'Lessons'
  ));

  AddConflict(TMultipleValuesConflict.Create('Teacher_MultForms',
    'Преподаватель ведет несколько разных форм занятий одновременно',
    sel, 'Lesson_Types'
  ));

  sel := 'Classrooms';
  AddConflict(TMultipleValuesConflict.Create('Classroom_MultTeachers',
    'В кабинете несколько преподавателей одновременно',
    sel, 'Teachers'
  ));

  AddConflict(TMultipleValuesConflict.Create('Classroom_MultLessons',
    'В кабинете преподают несколько дисциплин одновременно',
    sel, 'Lessons'
  ));

  AddConflict(TMultipleValuesConflict.Create('Classroom_MultTypes',
    'В кабинете ведутся несколько занятий разных форм одновременно',
    sel, 'Lesson_Types'
  ));

end.

