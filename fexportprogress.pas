unit fexportprogress;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ComCtrls,
  StdCtrls;

type

  { TFormExportProgress }

  TFormExportProgress = class(TForm)
    LabelState: TLabel;
    ProgressBar: TProgressBar;
  private
    { private declarations }
  public
    procedure UpdateProgress(m: string; p: integer);
    { public declarations }
  end;

var
  FormExportProgress: TFormExportProgress;

implementation

{$R *.lfm}

{ TFormExportProgress }

procedure TFormExportProgress.UpdateProgress(m: string; p: integer);
begin
  if m = 'FINISH' then begin
    self.Close;
    Release;
    exit();
  end;
  LabelState.Caption := m;
  ProgressBar.Position := p;
end;

end.

