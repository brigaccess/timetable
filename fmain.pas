unit fMain;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, IBConnection, sqldb, FileUtil, Forms, Controls,
  Graphics, Dialogs, Menus, StdCtrls, fView, metadata, ftimetable;

type

  { TMainForm }

  TMainForm = class(TForm)
    MagicButton: TButton;
    MainMenu: TMainMenu;
    MenuItemDirectory: TMenuItem;
    MenuItemQuit: TMenuItem;
    MenuItemAbout: TMenuItem;
    MenuItemFile: TMenuItem;
    procedure MagicButtonClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure MenuItemAboutClick(Sender: TObject);
    procedure MenuItemQuitClick(Sender: TObject);
    procedure MenuItemDirectoryItemClick(Sender: TObject);
    procedure ShowDirectoryPage(Page: integer);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  MainForm: TMainForm;

implementation

{$R *.lfm}

{ TMainForm }

procedure TMainForm.MenuItemAboutClick(Sender: TObject);
begin
  ShowMessage('Справочник v0.92, релиз "Экономический :\"' + #13#10 +
  'Бровцин Игорь, Б8103а-1');
end;

procedure TMainForm.FormCreate(Sender: TObject);
var
  i: integer;
  item: TMenuItem;
begin
  for i := 0 to High(metadata.RegisteredTables) do begin
    item := TMenuItem.Create(MenuItemDirectory);
    item.OnClick := @MenuItemDirectoryItemClick;
    item.Tag := i;
    item.Caption := metadata.RegisteredTables[i].Caption;
    MenuItemDirectory.Add(item);
  end;
end;

procedure TMainForm.MagicButtonClick(Sender: TObject);
begin
  TTimetableForm.Create(self).Show;
end;

procedure TMainForm.MenuItemQuitClick(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TMainForm.MenuItemDirectoryItemClick(Sender: TObject);
begin
  ShowDirectoryPage((Sender as TMenuItem).Tag);
end;

procedure TMainForm.ShowDirectoryPage(Page: integer);
var
  AForm: TViewForm;
begin
  AForm := TViewForm.Create(self, Page);
  AForm.Show;
end;

end.

