unit ExUtil;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, crt;

type
  aos = array of string; // :\

function SplitString(s: string; delim: Char): aos;
function repstr(s: string; times: integer): string;

implementation

function SplitString(s: string; delim: Char): aos;
var
  i, prev: integer;
begin
  prev := 0;
  for i := 1 to Length(s) do begin
    if (s[i] = delim) or (i = Length(s)) then begin
      SetLength(Result, Length(Result) + 1);
      if (i = Length(s)) then
        Result[High(Result)] := copy(s, prev + 1, i)
      else
        Result[High(Result)] := copy(s, prev + 1, i - prev - 1);
      prev := i;
    end;
  end;
end;

function repstr(s: string; times: integer): string;
var
  i: integer;
begin
  Result := s;
  for i := 1 to times do Result += s;
end;

end.

