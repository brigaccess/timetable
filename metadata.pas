unit metadata;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

type
  TTableInfo = class;

  TFieldType = (Numerical, Varchar, Time, Date);
//  TVarcharOps = ('LIKE', 'CONTAINING', 'SIMILAR TO');

  { TFieldInfo }

  TFieldInfo = class(TObject)
    private
      FTable: TTableInfo;
      FName: string;
      FCaption: string;
      FType: TFieldType;
      FWidth: integer;
    public
      constructor Create(Parent: TTableInfo; AName: string; ACaption: string;
        AFieldType: TFieldType; AWidth: integer);
      property Name: string read FName;
      property Caption: string read FCaption;
      property Table: TTableInfo read FTable;
      property FieldType: TFieldType read FType;
      property Width: integer read FWidth;
  end;


  TTableRelation = record
    Joined: TTableInfo;
    Left: string;
    Right: string;
  end;

  TTableConcat = record
    Fields: array of TFieldInfo;
    Separators: array of string;
  end;

  { TTableInfo }

  TTableInfo = class(TObject)
    private
      FName, FCaption: string;
      FRecordName, FSeparator: string;

      procedure AddDisplayField(AField: TFieldInfo);
    public
      ActualFields: array of TFieldInfo;
      DisplayFields: array of TFieldInfo;

      Relations: array of TTableRelation;
      Concats: array of TTableConcat;

      constructor Create(AName, ACaption, ARecordName: string); overload;
      constructor Create(AName, ACaption, ARecordName, ASeparator: string);
        overload;

      procedure AddField(AField: TFieldInfo);
      function AddField(AName, ACaption: string; AFieldtype: TFieldType;
        AWidth: integer): TFieldInfo;

      procedure AddForeignField(AField: TFieldInfo; Left, Right: string);
      procedure AddConcatFields(AFields: array of TFieldInfo;
        ASeparators: array of string);

      function GetSelectQuery(): string; overload;
      function GetSelectQuery(fields: array of string): string; overload;
      function GetSelectQuery(fields: array of string; AName: string): string;
        overload;
      function GetUpdateQuery(): string;
      function GetInsertQuery(): string;

      function GetField(AName: string): TFieldInfo;
      function GetFieldIndex(AName: string): integer;
      function GetRelation(AFieldname: string; var Res: TTableRelation): boolean;
      function GetRelationId(ATablename: string): integer;
      function GetRelatedDisplayField(ATablename: string): TFieldInfo;
      function GetRelatedDisplayFieldId(ATablename: string): integer;
      function GetRelatedActualField(ATablename: string): TFieldInfo;
      function GetConcatInfoId(AField: TFieldInfo): integer;

      property Name: string read FName;
      property Caption: string read FCaption;
      property RecordName: string read FRecordName;
      property Separator: string read FSeparator;
  end;

  TConflictRecord = record
    id, col, row: integer;  // Position of record in timetable
    date: TDate;
    category, details: string;
  end;

  TConflictRecords = array of TConflictRecord;

  TConflictTypeAbstract = class(TObject)
    private
      FName, FDescription: string;
    public
      constructor Create(AName, ADescription: string); virtual; abstract;
      function GetConflictRecords(rowname, colname: string;
        left, right: TDate): TConflictRecords; virtual; abstract;
      property Name: string read FName;
      property Description: string read FDescription;
  end;

  TTimetableItem = record
    id: integer;
    info: array of string;
    conflicts: array of TConflictTypeAbstract;
  end;

  TTimetableArray = array of array of array of TTimetableItem;
  PTimetableArray = ^TTimetableArray;

  TCenterOnField = procedure (col, row: integer) of object;

  const
    NumericalOps: array[0..4] of string = ('=', '>', '<', '>=', '<=');
    VarcharOps: array[0..7] of string = ('=', '>', '<', '>=', '<=', 'LIKE',
    'CONTAINING', 'SIMILAR TO');
  var
    RegisteredTables: array of TTableInfo;
    tbl: TTableInfo;
    sel: string;
    const TIMETABLENAME: string = 'Timetable';
  procedure RegisterTable(ATableInfo: TTableInfo);
  function GetTableInfo(ATableName: string): TTableInfo;
  function GetTableId(ATableName: string): integer;

implementation

procedure RegisterTable(ATableInfo: TTableInfo);
begin
  SetLength(RegisteredTables, Length(RegisteredTables) + 1);
  RegisteredTables[High(RegisteredTables)] := ATableInfo;
end;

function GetTableInfo(ATableName: string): TTableInfo;
var
  t: TTableInfo;
begin
  for t in RegisteredTables do begin
    if LowerCase(t.Name) = LowerCase(ATableName) then exit(t);
  end;
end;

function GetTableId(ATableName: string): integer;
var
  i: integer;
begin
  for i := 0 to High(RegisteredTables) do begin
    if Lowercase(RegisteredTables[i].Name) = Lowercase(ATableName) then exit(i);
  end;
  exit(-1);
end;

{ TTableInfo }

procedure TTableInfo.AddDisplayField(AField: TFieldInfo);
begin
  SetLength(DisplayFields, Length(DisplayFields) + 1);
  DisplayFields[High(DisplayFields)] := AField;
end;

function TTableInfo.GetConcatInfoId(AField: TFieldInfo): integer;
var
  i: integer;
begin
  for i := 0 to High(Concats) do begin
    if Concats[i].Fields[0] = AField then exit(i);
  end;
  exit(-1);
end;

function TTableInfo.GetRelationId(ATablename: string): integer;
var
  i: integer;
begin
  for i := 0 to High(Relations) do
    if Lowercase(Relations[i].Joined.Name) = Lowercase(ATablename) then exit(i);
  exit(-1);
end;

constructor TTableInfo.Create(AName, ACaption, ARecordName: string);
begin
  FName := AName;
  FCaption := ACaption;
  FRecordName := ARecordName;
  FSeparator := ' ';
end;

constructor TTableInfo.Create(AName, ACaption, ARecordName, ASeparator: string);
begin
  Create(AName, ACaption, ARecordName);
  FSeparator := ASeparator;
end;

procedure TTableInfo.AddField(AField: TFieldInfo);
begin
  Assert(AField.Table.Name = self.Name, 'Tried to add field from another table');
  SetLength(ActualFields, Length(ActualFields) + 1);
  ActualFields[High(ActualFields)] := AField;
end;

function TTableInfo.AddField(AName, ACaption: string; AFieldtype: TFieldType;
  AWidth: integer): TFieldInfo;
begin
  Result := TFieldInfo.Create(self, AName, ACaption, AFieldType, AWidth);
  AddField(Result);
  AddDisplayField(Result);
end;

procedure TTableInfo.AddForeignField(AField: TFieldInfo; Left, Right: string);
var
  i: integer;
  r: TTableRelation;
  related: boolean = false;
begin
  for i := 0 to High(Relations) do begin
    related := Relations[i].Joined.Name = AField.Table.Name;
    if related then break;
  end;
  if not related then begin
    SetLength(Relations, Length(Relations) + 1);
    r.Joined := AField.Table;
    r.Left := Left;
    r.Right := Right;
    Relations[High(Relations)] := r;
  end;
  AddDisplayField(AField);
end;

procedure TTableInfo.AddConcatFields(AFields: array of TFieldInfo;
  ASeparators: array of string);
var
  i: integer;
begin
  SetLength(Concats, Length(Concats) + 1);
  with Concats[High(Concats)] do begin
    SetLength(Fields, Length(AFields));
    for i := 0 to High(AFields) do Fields[i] := AFields[i];
    SetLength(Separators, Length(ASeparators));
    for i := 0 to High(ASeparators) do Separators[i] := ASeparators[i];
  end;
end;

function TTableInfo.GetSelectQuery: string;
begin
  exit(GetSelectQuery([]));
end;

function TTableInfo.GetSelectQuery(fields: array of string): string;
begin
  exit(GetSelectQuery(fields, TIMETABLENAME));
end;

function TTableInfo.GetSelectQuery(fields: array of string; AName: string
  ): string;
var
  i, j: integer;
  ti, ci, tci: integer;
begin
  Result := 'SELECT ';
  // Note: queries visible fields
  for i := 0 to High(fields) do begin
    Result += 't0.' + fields[i] + ', ';
  end;
  for i := 0 to High(DisplayFields) do begin
    if i > 0 then Result += ', ';
    ti := 0;
    if DisplayFields[i].Table.Name <> self.Name then
      ti := GetRelationId(DisplayFields[i].Table.Name) + 1;
    ci := GetConcatInfoId(DisplayFields[i]);

    if ci <> -1 then begin
      for j := 0 to High(Concats[ci].Fields) do begin
        with Concats[ci] do begin
          if j > 0 then begin
            Result += ' || ''';
            if j > High(Separators) then
              Result += Separators[High(Separators)]
            else
              Result += Separators[j];
            Result += ''' || ';
          end;
          tci := -1;
          if Fields[j].Table.Name <> self.Name then
            tci := GetRelationId(Fields[j].Table.Name);
          Result += Format('t%d.%s', [tci + 1, Fields[j].Name]);
        end;
      end;
    end
    else
      Result += Format('t%d.%s', [ti, DisplayFields[i].Name]);
  end;

  Result += Format(' FROM %s t0', [AName]);
  for i := 0 to High(Relations) do begin
    Result += Format(' LEFT JOIN %s t%d ON t0.%s = t%d.%s',
      [Relations[i].Joined.Name, i + 1,
       Relations[i].Left, i + 1,
       Relations[i].Right]
    );
  end;
end;

function TTableInfo.GetUpdateQuery: string;
var
  i: integer;
begin
  Result := Format('UPDATE %s t0 SET ', [self.Name]);
  for i := 1 to High(ActualFields) do begin
    if i > 1 then Result += ', ';
    Result += Format('%s = :prm%d', [ActualFields[i].Name, i]);
  end;

end;

function TTableInfo.GetInsertQuery: string;
var
  i: integer;
begin
  Result := Format('INSERT INTO %s VALUES (0, ', [self.Name]);
  for i := 1 to High(ActualFields) do begin
    if i > 1 then Result += ', ';
    Result += Format(':prm%d', [i]);
  end;
  Result += ')';
end;

function TTableInfo.GetField(AName: string): TFieldInfo;
var
  i: integer;
begin
  i := GetFieldIndex(AName);
  if i <> -1 then exit(ActualFields[i]);
  exit(Nil);
end;

function TTableInfo.GetFieldIndex(AName: string): integer;
var
  i: integer;
begin
  for i := 0 to High(ActualFields) do begin
    if Lowercase(ActualFields[i].Name) = Lowercase(AName) then exit(i)
  end;
  exit(-1);
end;

function TTableInfo.GetRelation(AFieldname: string; var Res: TTableRelation
  ): boolean;
var
  i: integer;
begin
  for i := 0 to High(Relations) do begin
    if Lowercase(Relations[i].Left) = Lowercase(AFieldname) then begin
      Res := Relations[i];
      exit(true);
    end;
  end;
  exit(false);
end;

function TTableInfo.GetRelatedDisplayField(ATablename: string): TFieldInfo;
var
  i: integer;
begin
  i := GetRelatedDisplayFieldId(ATablename);
  if i <> -1 then
    exit(DisplayFields[i])
  else
    exit(Nil);
end;

function TTableInfo.GetRelatedDisplayFieldId(ATablename: string): integer;
var
  i: integer;
begin
  for i := 0 to High(DisplayFields) do begin
    if Lowercase(DisplayFields[i].Table.Name) = Lowercase(ATablename) then
      exit(i);
  end;
  exit(-1);
end;

function TTableInfo.GetRelatedActualField(ATablename: string): TFieldInfo;
var
  i, rel: integer;
begin
  rel := GetRelationId(ATablename);
  if (rel = -1) then exit(Nil);
  for i := 0 to High(ActualFields) do begin
    if Lowercase(ActualFields[i].Name) = Lowercase(Relations[rel].Left) then
      exit(ActualFields[i]);
  end;
  exit(Nil);
end;

{ TFieldInfo }

constructor TFieldInfo.Create(Parent: TTableInfo; AName: string;
  ACaption: string; AFieldType: TFieldType; AWidth: integer);
begin
  FTable := Parent;
  FName := LowerCase(AName);
  FCaption := ACaption;
  FType := AFieldType;
  FWidth := AWidth;
end;

initialization
  tbl := TTableInfo.Create('Groups', 'Группы', 'Группа');
  tbl.AddField('id', 'ID', Numerical, 50);
  tbl.AddField('name', 'Группа', Varchar, 100);
  tbl.AddField('students_count', 'Количество учеников', Numerical, 160);
  RegisterTable(tbl);

  tbl := TTableInfo.Create('Lessons', 'Дисциплины', 'Дисциплина');
  tbl.AddField('id', 'ID', Numerical, 50);
  tbl.AddField('name', 'Дисциплина', Varchar, 440);
  RegisterTable(tbl);

  tbl := TTableInfo.Create('Teachers', 'Преподаватели', 'Преподаватель');
  tbl.AddField('id', 'ID', Numerical, 50);
  tbl.AddField('last_name', 'Фамилия', Varchar, 350);
  tbl.AddField('first_name', 'Имя', Varchar, 230);
  tbl.AddField('middle_name', 'Отчество', Varchar, 230);
  RegisterTable(tbl);

  tbl := TTableInfo.Create('Classrooms', 'Кабинеты', 'Кабинет');
  tbl.AddField('id', 'ID', Numerical, 50);
  tbl.AddField('name', 'Кабинет', Varchar, 120);
  tbl.AddField('capacity', 'Вместимость', Numerical, 100);
  RegisterTable(tbl);

  tbl := TTableInfo.Create('Weekdays', 'Дни недели', 'День недели');
  tbl.AddField('id', 'ID', Numerical, 50);
  tbl.AddField('name', 'День недели', Varchar, 110);
  RegisterTable(tbl);

  tbl := TTableInfo.Create('Lesson_Times', 'Время занятий', 'Время занятия',
    ' - ');
  tbl.AddField('id', 'ID', Numerical, 50);
  tbl.AddField('begin_', 'Начало занятия', Varchar, 120);
  tbl.AddField('end_', 'Конец занятия', Varchar, 120);
  RegisterTable(tbl);

  tbl := TTableInfo.Create('Lesson_Types', 'Формы занятий', 'Форма занятия');
  tbl.AddField('id', 'ID', Numerical, 50);
  tbl.AddField('name', 'Форма', Varchar, 210);
  RegisterTable(tbl);

  tbl := TTableInfo.Create('Timetable', 'Расписание', 'Занятие');
  tbl.AddField('id', 'ID', Numerical, 50);
  // 'Invisible' fields for FK
  tbl.AddField(TFieldInfo.Create(tbl, 'Lesson_id', 'ID', Numerical, 50));
  tbl.AddField(TFieldInfo.Create(tbl, 'Lesson_Type_id', 'ID', Numerical, 50));
  tbl.AddField(TFieldInfo.Create(tbl, 'Teacher_id', 'ID', Numerical, 50));
  tbl.AddField(TFieldInfo.Create(tbl, 'Group_id', 'ID', Numerical, 50));
  tbl.AddField(TFieldInfo.Create(tbl, 'Classroom_id', 'ID', Numerical, 50));
  tbl.AddField(TFieldInfo.Create(tbl, 'Weekday_id', 'ID', Numerical, 50));
  tbl.AddField(TFieldInfo.Create(tbl, 'Lesson_time_id', 'ID', Numerical, 50));

  tbl.AddForeignField(GetTableInfo('Lessons').GetField('name'),
    'Lesson_id', 'id');
  tbl.AddForeignField(GetTableInfo('Lesson_Types').GetField('name'),
    'Lesson_Type_id', 'id');
  tbl.AddForeignField(GetTableInfo('Teachers').GetField('last_name'),
    'Teacher_id', 'id');
  with GetTableInfo('Teachers') do begin
    tbl.AddConcatFields([GetField('last_name'), GetField('first_name'),
      GetField('middle_name')], [' ']);
  end;
  tbl.AddForeignField(GetTableInfo('Groups').GetField('name'),
    'Group_id', 'id');
  with GetTableInfo('Groups') do begin
    tbl.AddConcatFields([GetField('name'), GetField('students_count')], [' ']);
  end;
  tbl.AddForeignField(GetTableInfo('Classrooms').GetField('name'),
    'Classroom_id', 'id');
  with GetTableInfo('Classrooms') do begin
    tbl.AddConcatFields([GetField('name'), GetField('capacity')], [' ']);
  end;
  tbl.AddForeignField(GetTableInfo('Weekdays').GetField('name'),
    'Weekday_id', 'id');
  tbl.AddForeignField(GetTableInfo('Lesson_Times').GetField('begin_'),
    'Lesson_time_id', 'id');
  tbl.AddForeignField(GetTableInfo('Lesson_Times').GetField('end_'),
    'Lesson_time_id', 'id');

  tbl.AddField('start_date', 'Актуально с', Date, 90);
  tbl.AddField('end_date', 'Актуально до', Date, 100);
  tbl.AddField('period', 'Периодичность', Numerical, 120);

  RegisterTable(tbl);

end.

