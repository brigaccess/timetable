unit fCard;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  StdCtrls, DBGrids, metadata, sqldb, db, dbprovider, eventnotify, fieldeditors,
  contnrs, LCLType, Buttons, ExUtil;

type
  { TEditForm }

  TEditForm = class(TForm, IListener)
    ButtonClose: TButton;
    ButtonSave: TButton;
    DataSource: TDataSource;
    PanelContainer: TPanel;
    SpeedButtonUnlock: TSpeedButton;
    SQLQuery: TSQLQuery;
    SQLTransaction: TSQLTransaction;
    procedure ButtonCloseClick(Sender: TObject);
    procedure ButtonSaveClick(Sender: TObject);
    constructor Create(TheOwner: TComponent; Table: TTableInfo; Recno: integer);
    procedure FormCloseQuery(Sender: TObject; var CanClose: boolean);
    procedure ForceClose();
    destructor Destroy; override;
    procedure PanelContainerClick(Sender: TObject);
    procedure SetIntValue(ATable, AField: String; AValue: integer);
    procedure SetStrValue(ATable, AField, AValue: String);
    procedure SpeedButtonUnlockClick(Sender: TObject);
    procedure UpdateData();
  public
    HasChanged: boolean;
  private
    EditorPanels: TObjectList;
    FTable: TTableInfo;
    FRecno: integer;
    LockedTables: array of String;
    Unlocked: boolean;
    procedure HandleMessage(Channel, Event, Details: string);
    procedure UpdateLock();
    function GetEditorByField(ATable, AField: String): TFieldEditorBase;
  public
    { public declarations }
  end;

  TCard = record
    ID: String;
    Form: TEditForm;
  end;

  TCards = array of TCard;

var
  EditForm: TEditForm;
  Cards: TCards;
  procedure EditItem(Parent: TComponent; Tablename: String; rid: integer);
  function GetCard(Tablename: String; rid: integer): TEditForm;

implementation

procedure EditItem(Parent: TComponent; Tablename: String; rid: integer);
var
  i: integer;
  id: String;
  c: TCard;
begin
  id := LowerCase(Tablename) + IntToStr(rid);
  for i := 0 to High(Cards) do begin
    if Cards[i].ID = id then begin
      Cards[i].Form.Show();
      exit();
    end;
  end;
  c.ID := id;
  c.Form := TEditForm.Create(Parent, GetTableInfo(Tablename), rid);
  SetLength(Cards, Length(Cards) + 1);
  Cards[High(Cards)] := c;
  c.Form.Show();
end;

function GetCard(Tablename: String; rid: integer): TEditForm;
var
  i: integer;
  id: String;
  c: TCard;
begin
  id := LowerCase(Tablename) + IntToStr(rid);
  for i := 0 to High(Cards) do begin
    if Cards[i].ID = id then begin
      exit(Cards[i].Form);
    end;
  end;
  exit(Nil);
end;

{$R *.lfm}

{ TEditForm }

constructor TEditForm.Create(TheOwner: TComponent; Table: TTableInfo;
  Recno: integer);
var
  i, j: integer;
  fe: TFieldEditorBase;
begin
  inherited Create(TheOwner);
  EditorPanels := TObjectList.Create();
  FTable := Table;
  FRecno := Recno;
  Unlocked := False;
  HasChanged := False;
  eventnotify.GetHub(Table.Name + 'card' + IntToStr(Recno)).RegisterListener(self);
  eventnotify.GetHub(Table.Name + 'card').RegisterListener(self);
  eventnotify.GetHub(Table.Name).RegisterListener(self);

  with Table do begin
    for i := 0 to High(DisplayFields) do begin
      if Lowercase(DisplayFields[i].Table.Name) <> Lowercase(Table.Name) then
      begin
        fe := TFieldForeignEditor.Create(self, DisplayFields[i], Recno, Table);
        eventnotify.GetHub(DisplayFields[i].Table.Name).RegisterListener(
          TFieldForeignEditor(fe)
        );
      end
      else begin
        if DisplayFields[i].FieldType = Date then
           fe := TFieldDateEditor.Create(self, DisplayFields[i], Recno, Table)
        else
           fe := TFieldEditor.Create(self, DisplayFields[i], Recno, Table);
      end;

      if i = 0 then fe.Enabled := false;
      EditorPanels.Add(fe);
      PanelContainer.InsertControl(fe);
    end;
  end;

  SQLTransaction.DataBase := dbprovider.DbData.DBConnection;
  SQLQuery.Transaction := SQLTransaction;
  DataSource.DataSet := SQLQuery;

  UpdateData();

  self.Height := 12 + 36 * (i + 1) + 42;
  self.Caption := Table.Caption + ': редактирование';
  eventnotify.GetHub(Table.Name).Send('CardOpened', IntToStr(Recno));
end;

procedure TEditForm.FormCloseQuery(Sender: TObject; var CanClose: boolean);
var
  ans: integer;
  i: integer;
begin
  if (HasChanged) then begin
    ans := Application.MessageBox(
      'Сохранить изменения?',
      'Изменения',
      MB_YESNOCANCEL + MB_ICONQUESTION
    );
    if (ans = IDCANCEL) then
      CanClose := False
    else if (ans = IDYES) then ButtonSave.Click;
  end;
end;

procedure TEditForm.ForceClose;
begin
  HasChanged := False;
  Close;
end;

destructor TEditForm.Destroy;
begin
  eventnotify.GetHub(FTable.Name + 'card' + IntToStr(FRecno)).Handlers.Remove(self);
  inherited Destroy;
end;

procedure TEditForm.PanelContainerClick(Sender: TObject);
begin

end;

procedure TEditForm.SetIntValue(ATable, AField: String; AValue: integer);
var
  i: integer;
  fe: TFieldEditorBase;
begin
  for i := 0 to EditorPanels.Count - 1 do begin
    fe := TFieldEditorBase(EditorPanels.Items[i]);
    if (fe.FieldInfo.Name = AField) and (fe.FieldInfo.Table.Name = ATable) then
      fe.SetIntValue(AValue, True);
  end;
end;

procedure TEditForm.SetStrValue(ATable, AField, AValue: String);
var
  i: integer;
  fe: TFieldEditorBase;
begin
  for i := 0 to EditorPanels.Count - 1 do begin
    fe := TFieldEditorBase(EditorPanels.Items[i]);
    if (fe.FieldInfo.Name = AField) and (fe.FieldInfo.Table.Name = ATable) then
      fe.SetStrValue(AValue, True);
  end;
end;

procedure TEditForm.SpeedButtonUnlockClick(Sender: TObject);
begin
  Unlocked := True;
  UpdateLock();
  SpeedButtonUnlock.Enabled := False;
end;

procedure TEditForm.UpdateData;
var
  i: integer;
begin
  if FRecno <> -1 then begin
    SQLTransaction.Commit;
    SQLQuery.Close;
    SQLQuery.SQL.Clear;
    SQLQuery.SQL.Add(FTable.GetSelectQuery());
    SQLQuery.SQL.Add('WHERE t0.id = ' + IntToStr(FRecno));
    SQLQuery.Open;

    for i := 0 to High(FTable.DisplayFields) do begin
      if FTable.DisplayFields[i].FieldType = Numerical then begin
        TFieldEditorBase(EditorPanels[i]).SetIntValue(
          DataSource.DataSet.Fields.Fields[i].AsInteger, True
        );
      end
      else if FTable.DisplayFields[i].FieldType = Varchar then begin
        TFieldEditorBase(EditorPanels[i]).SetStrValue(
          DataSource.DataSet.Fields.Fields[i].AsString, True
        );
      end
      else if FTable.DisplayFields[i].FieldType = Date then begin
        TFieldEditorBase(EditorPanels[i]).SetStrValue(
          DateToStr(DataSource.DataSet.Fields.Fields[i].AsDateTime), True
        );
      end;
    end;
    HasChanged := False;
  end;
end;

procedure TEditForm.HandleMessage(Channel, Event, Details: string);
begin
  if (Event = 'SetLock') then begin
    if Unlocked then exit;
    LockedTables := ExUtil.SplitString(Details, ';');
    if not Unlocked then begin
      SpeedButtonUnlock.Visible := True;
    end;
    UpdateLock();
  end
  else if (Event = 'Change') then begin
    HasChanged := True;
    ButtonSave.Enabled := True;
  end
  else if (Event = 'Update') then begin
    UpdateData();
  end;
end;

procedure TEditForm.UpdateLock;
var
  i, j: integer;
  p: TFieldEditorBase;
begin
  if (Length(LockedTables) = 0) or Unlocked then begin
    for i := 1 to EditorPanels.Count - 1 do begin
      TFieldEditorBase(EditorPanels[i]).Enabled := True;
    end;
  end
  else begin
    for i := 1 to EditorPanels.Count - 1 do begin
      p := TFieldEditorBase(EditorPanels[i]);
      for j := 0 to High(LockedTables) do begin
        if p.FieldInfo.Table.Name = LockedTables[j] then begin
          p.Enabled := False;
          break;
        end;
      end;
    end;
  end;
end;

function TEditForm.GetEditorByField(ATable, AField: String): TFieldEditorBase;
var
  i: integer;
begin
  for i := 0 to EditorPanels.Count - 1 do begin
    with TFieldEditorBase(EditorPanels[i]) do begin
      if Lowercase(ATable) <> Lowercase(self.FTable.Name) then begin
        if Lowercase(FieldInfo.Table.Name) = Lowercase(ATable) then
          exit(TFieldEditorBase(EditorPanels[i]))
      end
      else if Lowercase(FieldInfo.Name) = Lowercase(AField) then
        exit(TFieldEditorBase(EditorPanels[i]));
    end;
  end;
  exit(Nil);
end;

procedure TEditForm.ButtonSaveClick(Sender: TObject);
var
  i, j: integer;
  p: TParam;
  c: TTableRelation;
  fe: TFieldEditorBase;
begin
   SQLQuery.Close;
   SQLQuery.SQL.Clear;
   SQLQuery.InsertSQL.Clear;
   SQLQuery.UpdateSQL.Clear;

   if FRecno = -1 then begin
      SQLQuery.SQL.Text := FTable.GetInsertQuery();
   end
   else begin
      SQLQuery.SQL.Text := FTable.GetUpdateQuery() + ' WHERE t0.id = :prmid';
      SQLQuery.ParamByName('prmid').AsInteger := FRecno;
   end;

   for i := 1 to High(FTable.ActualFields) do begin
      p := SQLQuery.Params.ParamByName(Format('prm%d', [i]));
      if FTable.GetRelation(FTable.ActualFields[i].Name, c) then begin
        fe := GetEditorByField(c.Joined.Name, c.Right);
      end
      else begin
        fe := GetEditorByField(FTable.Name, FTable.ActualFields[i].Name);
      end;

      if FTable.ActualFields[i].FieldType = Varchar then
        p.AsString := fe.GetStrValue()
      else if FTable.ActualFields[i].FieldType = Numerical then
        p.AsInteger := fe.GetIntValue()
      else if FTable.ActualFields[i].FieldType = Date then
        p.AsDateTime := StrToDate(fe.GetStrValue());
   end;

   try
    SQLQuery.ExecSQL;
    SQLTransaction.Commit;
    if ProviderExists(FTable.Name) then
      eventnotify.GetHub(FTable.Name).Send('UpdateProviders', '')
    else
      eventnotify.GetHub(FTable.Name).Send('Update', '');
    ButtonSave.Enabled := False;
    HasChanged := False;
    ShowMessage('Изменения сохранены');
   except on e: Exception do
    ShowMessage('Ошибка базы данных: ' + e.Message);
   end;
end;

procedure TEditForm.ButtonCloseClick(Sender: TObject);
begin
  self.Close;
end;

end.

