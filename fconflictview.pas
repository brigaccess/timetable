unit fconflictview;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, DateTimePicker, Forms, Controls, Graphics,
  Dialogs, ComCtrls, StdCtrls, Buttons, eventnotify, metadata, dbprovider,
  conflicts;

type

  TConflictTreeItemData = object
    id: integer;
    col: integer;
    row: integer;
  end;

  { TConflictViewForm }

  TConflictViewForm = class(TForm, IListener)
    DatePickerLeft: TDateTimePicker;
    DatePickerRight: TDateTimePicker;
    GroupBoxPeriod: TGroupBox;
    LabelLeft: TLabel;
    LabelRight: TLabel;
    SpeedButtonApply: TSpeedButton;
    TreeView: TTreeView;
    constructor Create(TheOwner: TComponent; rowptr, colptr: PInteger;
      CenterMethod: TCenterOnField);
    procedure HandleMessage(Channel, Message, Details: string);
    procedure FillConflicts(t: TConflictType);
    procedure SpeedButtonApplyClick(Sender: TObject);
    procedure TreeViewDblClick(Sender: TObject);
    procedure TreeViewDeletion(Sender: TObject; Node: TTreeNode);
    procedure NavigateTo(Description: string; col, row: integer);
    function FindOrAdd(Node: TTreeNode; Str: string): TTreeNode;
  private
    { private declarations }
    FCurCols, FCurRows: PInteger;
    FCenter: TCenterOnField;
    function FindSibling(Node: TTreeNode; Search: string): TTreeNode;
    function FindChild(Node: TTreeNode; Search: string): TTreeNode;
  public
    { public declarations }
  end;

var
  ConflictViewForm: TConflictViewForm;

implementation

{$R *.lfm}

{ TConflictViewForm }

constructor TConflictViewForm.Create(TheOwner: TComponent; rowptr,
  colptr: PInteger; CenterMethod: TCenterOnField);
begin
  inherited Create(TheOwner);

  FCenter := CenterMethod;
  FCurCols := colptr;
  FCurRows := rowptr;
  eventnotify.GetHub('Timetable').RegisterListener(self);
end;

procedure TConflictViewForm.HandleMessage(Channel, Message, Details: string);
var
  i: integer;
begin
  if Message <> 'UpdateConflicts' then exit();
  if (FCurCols = Nil) or (FCurRows = Nil) then begin
    New(FCurCols);
    FCurCols^ := 0;
    New(FCurRows);
    FCurRows^ := 1;
  end;
  TreeView.Items.BeginUpdate;
  TreeView.Items.Clear;

  for i := 0 to High(conflicts.ConflictTypes) do begin
    TreeView.Items.Add(nil, conflicts.ConflictTypes[i].Description);
  end;

  conflicts.ConflictManager.GetConflicts(@self.FillConflicts);

  TreeView.Items.EndUpdate;
end;

procedure TConflictViewForm.FillConflicts(t: TConflictType);
var
  d: TConflictRecords;
  i: integer;
  tt: TTableInfo;
  rowname, colname: string;
  root, child: TTreeNode;
  ctid: ^TConflictTreeItemData;
begin
  tt := GetTableInfo(TIMETABLENAME);
  rowname := tt.GetRelatedActualField(RegisteredTables[FCurRows^].Name).Name;
  colname := tt.GetRelatedActualField(RegisteredTables[FCurCols^].Name).Name;
  root := FindSibling(TreeView.Items.GetFirstNode, t.Description);
  if root = Nil then
    raise Exception.Create('No root node for ConflictType');
  d := t.GetConflictRecords(rowname, colname,
    DatePickerLeft.Date, DatePickerRight.Date);
  for i := 0 to High(d) do begin
    child := FindOrAdd(root, DateToStr(d[i].date));
    child := FindOrAdd(child, d[i].category);
    child := TreeView.Items.AddChild(child, d[i].details);
    New(ctid);
    ctid^.col := d[i].col;
    ctid^.row := d[i].row;
    ctid^.id := d[i].id;
    child.Data := ctid;
  end;
end;

procedure TConflictViewForm.SpeedButtonApplyClick(Sender: TObject);
begin
  HandleMessage('', 'UpdateConflicts', '');
end;

procedure TConflictViewForm.TreeViewDblClick(Sender: TObject);
var
  ctidp: ^TConflictTreeItemData;
  ctid: TConflictTreeItemData;
begin
  if (TreeView.Selected = Nil) or (TreeView.Selected.Parent = Nil) then exit();
  if (TreeView.Selected.Level = 3) and (FCurCols <> Nil) and (FCurRows <> Nil)
  then begin
    ctidp := TreeView.Selected.Data;
    ctid := (ctidp)^;
    FCenter(ctid.col, ctid.row);
  end;
end;

procedure TConflictViewForm.TreeViewDeletion(Sender: TObject; Node: TTreeNode);
var
  ctidp: ^TConflictTreeItemData;
begin
  if (Node = Nil) or (Node.Parent = Nil) or (Node.Level < 2) then exit();
  ctidp := Node.Data;
  Dispose(ctidp);
end;

procedure TConflictViewForm.NavigateTo(Description: string; col, row: integer);
var
  n, x: TTreeNode;
  d: ^TConflictTreeItemData;
  i, last: integer;
begin
  n := FindSibling(TreeView.Items.GetFirstNode, Description);
  if not n.HasChildren then exit();
  last := n.GetLastChild.AbsoluteIndex;
  x := n.GetFirstChild;
  i := x.AbsoluteIndex;
  while (i < last) do begin
    d := x.Data;
    if (d <> nil) and (d^.col = col) and (d^.row = row) then begin
      TreeView.Selected := x;
      break;
    end;
    x := x.GetNext;
    i += 1;
  end;
end;

function TConflictViewForm.FindOrAdd(Node: TTreeNode; Str: string
  ): TTreeNode;
begin
  Result := FindChild(Node, Str);
  if Result = Nil then
    Result := TreeView.Items.AddChild(Node, Str);
end;

function TConflictViewForm.FindSibling(Node: TTreeNode; Search: string
  ): TTreeNode;
var
  n: TTreeNode;
begin
  n := Node;
  while (n <> Nil) and (Search <> n.Text) do
    n := n.GetNextSibling;
  exit(n);
end;

function TConflictViewForm.FindChild(Node: TTreeNode; Search: string
  ): TTreeNode;
var
  n: TTreeNode;
begin
  n := Node.GetFirstChild;
  exit(FindSibling(n, Search));
end;

end.

