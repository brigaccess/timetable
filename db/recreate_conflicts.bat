@echo off
echo Killing server
taskkill /F /IM fbserver.exe
echo Removing old database
del .\TIMETABLE.fdb
echo Starting server
start fbserver.exe -a
echo Running init script
isql -i script_conflicts.sql