CREATE DATABASE 'localhost:timetable' USER 'SYSDBA' PASSWORD 'masterkey' DEFAULT CHARACTER SET UTF8;
CREATE GENERATOR GroupsIdGenerator;
CREATE GENERATOR LessonsIdGenerator;
CREATE GENERATOR TeachersIdGenerator;
CREATE GENERATOR ClassRoomsIdGenerator;
CREATE GENERATOR LessonTimesIdGenerator;
CREATE GENERATOR WeekDaysIdGenerator;
CREATE GENERATOR TimeTableIdGenerator;
CREATE GENERATOR LessonTypesIdGenerator;

CREATE TABLE Groups(
	id INTEGER NOT NULL PRIMARY KEY,
	name VARCHAR(100),
	students_count INTEGER
);

CREATE TABLE Lessons(
	id INTEGER NOT NULL PRIMARY KEY,
	name VARCHAR(100)
);

CREATE TABLE Teachers(
	id INTEGER NOT NULL PRIMARY KEY,
	last_name VARCHAR(100),
	first_name VARCHAR(100), 
   middle_name VARCHAR(100)
);

CREATE TABLE Classrooms(
	id INTEGER NOT NULL PRIMARY KEY,
	name VARCHAR(100),
	capacity INTEGER
);

CREATE TABLE Lesson_Times(
	id INTEGER NOT NULL PRIMARY KEY,
	begin_ TIME,
	end_ TIME,
	sorting_field INTEGER UNIQUE
);

CREATE TABLE Weekdays(
	id INTEGER NOT NULL PRIMARY KEY,
	name VARCHAR(100),
	sorting_field INTEGER UNIQUE
);

CREATE TABLE Lesson_Types(
	id INTEGER NOT NULL PRIMARY KEY,
	name VARCHAR(100)
);

CREATE TABLE Timetable(
	id INTEGER NOT NULL PRIMARY KEY,
	lesson_id INTEGER REFERENCES Lessons(id),
	lesson_type_id INTEGER REFERENCES Lesson_Types(id),
	teacher_id INTEGER REFERENCES Teachers(id),
	group_id INTEGER REFERENCES Groups(id),
	classroom_id INTEGER REFERENCES Classrooms(id),
	weekday_id INTEGER REFERENCES Weekdays(id),
	lesson_time_id INTEGER REFERENCES Lesson_Times(id)
);

SET TERM ^ ;

CREATE TRIGGER GroupsIdTrigger FOR Groups ACTIVE BEFORE INSERT
AS BEGIN
new.id = NEXT VALUE FOR GroupsIdGenerator;
END^

CREATE TRIGGER TeachersIdTrigger FOR Teachers ACTIVE BEFORE INSERT
AS BEGIN
new.id = NEXT VALUE FOR TeachersIdGenerator;
END^

CREATE TRIGGER LessonsIdTrigger FOR Lessons ACTIVE BEFORE INSERT
AS BEGIN
new.id = NEXT VALUE FOR LessonsIdGenerator;
END^

CREATE TRIGGER LessonTimesIdTrigger FOR Lesson_Times ACTIVE BEFORE INSERT
AS BEGIN
new.id = NEXT VALUE FOR LessonTimesIdGenerator;
END^

CREATE TRIGGER WeekDaysIdTrigger FOR Weekdays ACTIVE BEFORE INSERT
AS BEGIN
new.id = NEXT VALUE FOR WeekDaysIdGenerator;
END^

CREATE TRIGGER ClassRoomsIdTrigger FOR Classrooms ACTIVE BEFORE INSERT
AS BEGIN
new.id = NEXT VALUE FOR ClassRoomsIdGenerator;
END^

CREATE TRIGGER LessonTypesIdTrigger FOR Lesson_Types ACTIVE BEFORE INSERT
AS BEGIN
new.id = NEXT VALUE FOR LessonTypesIdGenerator;
END^

CREATE TRIGGER TimeTableIdTrigger FOR Timetable ACTIVE BEFORE INSERT
AS BEGIN
new.id = NEXT VALUE FOR TimeTableIdGenerator;
END^

SET TERM ; ^

COMMIT;
INSERT INTO Lesson_Times VALUES(1, '08:30:00.0000', '10:00:00.0000', 1);
INSERT INTO Lesson_Times VALUES(2, '10:10:00.0000', '11:40:00.0000', 2);
INSERT INTO Lesson_Times VALUES(3, '11:50:00.0000', '13:20:00.0000', 3);
INSERT INTO Lesson_Times VALUES(4, '13:30:00.0000', '15:00:00.0000', 4);
INSERT INTO Lesson_Times VALUES(5, '15:10:00.0000', '16:40:00.0000', 5);
INSERT INTO Lesson_Times VALUES(6, '16:50:00.0000', '18:20:00.0000', 6);
INSERT INTO Lesson_Times VALUES(7, '18:30:00.0000', '20:00:00.0000', 7);
INSERT INTO Lesson_Times VALUES(8, '20:10:00.0000', '21:40:00.0000', 8);

INSERT INTO Weekdays VALUES(1, 'Понедельник', 1);
INSERT INTO Weekdays VALUES(2, 'Вторник', 2);
INSERT INTO Weekdays VALUES(3, 'Среда', 3);
INSERT INTO Weekdays VALUES(4, 'Четверг', 4);
INSERT INTO Weekdays VALUES(5, 'Пятница', 5);
INSERT INTO Weekdays VALUES(6, 'Суббота', 6);
INSERT INTO Weekdays VALUES(7, 'Воскресенье', 7);

INSERT INTO Lesson_Types VALUES(1, 'DUMMY_TYPE');
INSERT INTO Lesson_Types VALUES(2, 'Multiple_Types_1');
INSERT INTO Lesson_Types VALUES(3, 'Multiple_Types_2');

INSERT INTO Groups VALUES(1, 'DUMMY_GROUP', 5);
INSERT INTO Groups VALUES(2, 'Group_Multiple_Classrooms', 5);
INSERT INTO Groups VALUES(3, 'Group_Multiple_Lessons', 5);
INSERT INTO Groups VALUES(4, 'Group_Multiple_Types', 5);
INSERT INTO Groups VALUES(5, 'Group_Multiple_Teachers', 5);

INSERT INTO Lessons VALUES(1, 'DUMMY_LESSON');
INSERT INTO Lessons VALUES(2, 'Multiple_Lessons_1');
INSERT INTO Lessons VALUES(3, 'Multiple_Lessons_2');

INSERT INTO Teachers VALUES(1, 'DUMMY_TEACHER', 'N.', 'N.');
INSERT INTO Teachers VALUES(2, 'Multiple_Teachers_1', 'A.', 'A.');
INSERT INTO Teachers VALUES(3, 'Multiple_Teachers_2', 'B.', 'B.');
INSERT INTO Teachers VALUES(4, 'Teacher_Multiple_Classrooms', 'C.', 'C.');
INSERT INTO Teachers VALUES(5, 'Teacher_Multiple_Lessons', 'L.', 'L.');
INSERT INTO Teachers VALUES(6, 'Teacher_Multiple_Types', 'L.', 'L.');

INSERT INTO Classrooms VALUES(1, 'DUMMY_CLASSROOM', 20);
INSERT INTO Classrooms VALUES(2, 'Multiple_Classrooms_1', 20);
INSERT INTO Classrooms VALUES(3, 'Multiple_Classrooms_2', 20);
INSERT INTO Classrooms VALUES(4, 'Multiple_Teachers_Classroom', 20);
INSERT INTO Classrooms VALUES(5, 'Multiple_Lessons_Classroom', 20);
INSERT INTO Classrooms VALUES(6, 'Multiple_Types_Classroom', 20);
INSERT INTO Classrooms VALUES(7, 'Small_Classroom', 1);

-- id, lesson_id, lesson_type_id, teacher_id, group_id, classroom_id, weekday_id, lesson_time_id

-- Multiple classrooms (Group)
-- Multiple classrooms (Teacher)
INSERT INTO Timetable VALUES(1, 1, 1, 4, 2, 2, 1, 1);
INSERT INTO Timetable VALUES(2, 1, 1, 4, 2, 3, 1, 1);

-- Multiple lessons (Group)
-- Multiple lessons (Teacher)
-- Multiple lessons (Classroom)
INSERT INTO Timetable VALUES(3, 2, 1, 5, 3, 5, 1, 2);
INSERT INTO Timetable VALUES(4, 3, 1, 5, 3, 5, 1, 2);

-- Multiple types (Group)
-- Multiple types (Teacher)
-- Multiple types (Classroom)
INSERT INTO Timetable VALUES(5, 1, 2, 6, 4, 1, 1, 3);
INSERT INTO Timetable VALUES(6, 1, 3, 6, 4, 1, 1, 3);

-- Multiple teachers (Group)
-- Multiple teachers (Classroom)
INSERT INTO Timetable VALUES(7, 1, 1, 2, 5, 4, 1, 3);
INSERT INTO Timetable VALUES(8, 1, 1, 3, 5, 4, 1, 3);

-- Small classroom
INSERT INTO Timetable VALUES(9, 1, 1, 1, 1, 7, 1, 4);
COMMIT;
SET TERM ^ ;
CREATE PROCEDURE Split_string(
    P_STRING VARCHAR(1024),
    P_SPLITTER CHAR(1)) 
RETURNS(
    PART VARCHAR(1024)
) 
AS
  DECLARE VARIABLE LASTPOS INTEGER;
  DECLARE VARIABLE NEXTPOS INTEGER;
BEGIN
    P_STRING = :P_STRING || :P_SPLITTER;
    LASTPOS = 1;
    NEXTPOS = POSITION(:P_SPLITTER, :P_STRING, LASTPOS);
    IF (LASTPOS = NEXTPOS) THEN
        BEGIN
            PART = SUBSTRING(:P_STRING FROM :LASTPOS FOR :NEXTPOS - :LASTPOS);
            SUSPEND;
            LASTPOS = :NEXTPOS + 1;
            NEXTPOS = position(:P_SPLITTER, :P_STRING, LASTPOS);
        END
    WHILE (:NEXTPOS > 1) DO
        BEGIN
            PART = SUBSTRING(:P_STRING FROM :LASTPOS FOR :NEXTPOS - :LASTPOS);
            LASTPOS = :NEXTPOS + 1;
            NEXTPOS = position(:P_SPLITTER, :P_STRING, LASTPOS);
            SUSPEND;
        END
END^
SET TERM ; ^
COMMIT;
/*id lesson_id lesson_type_id teacher_id group_id class_room_id week_day_id lesson_time_id*/
/*SELECT tt.ID, l.NAME AS Lesson_Name, lt.NAME AS Lesson_Type, 
        t.LAST_NAME AS Teacher_Last_Name, t.FIRST_NAME AS Teacher_First_Name, 
        t.MIDDLE_NAME AS Teacher_Middle_Name, g.NAME AS Group_Name, cr.NAME AS CLASSROOM,
        wd.NAME AS WEEKDAY, lti.BEGIN_ AS Lesson_Start, lti.END_ AS Lesson_End FROM Timetable tt 
    INNER JOIN LESSONS l ON tt.LESSON_ID = l.ID 
    INNER JOIN LESSONS_TYPES lt ON tt.LESSON_TYPE_ID = lt.ID
    INNER JOIN TEACHERS t ON tt.TEACHER_ID = t.ID
    INNER JOIN GROUPS g ON tt.GROUP_ID = g.ID
    INNER JOIN Classrooms cr ON tt.CLASSROOM_ID = cr.ID
    INNER JOIN Weekdays wd ON tt.WEEKDAY_ID = wd.ID
    INNER JOIN LESSONS_TIMES lti ON tt.LESSON_TIME_ID = lti.ID
    ORDER BY wd.ID, lt.ID DESCENDING, tt.ID;*/