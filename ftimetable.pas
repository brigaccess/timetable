unit ftimetable;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, DateTimePicker, Forms, Controls, Graphics,
  Dialogs, Grids, StdCtrls, Buttons, ExtCtrls, metadata, dbprovider,
  eventnotify, sqldb, DB, Math, LCLType, DBGrids, Menus, variants, fCard, fView,
  filters, contnrs, fconflictview, conflicts, ExUtil, exportmanager, dateutils;

type

  TItemButton = (NONE, NEWWIN, CONFLICT, ADD, EDIT, DELETE);
  TItemButtons = set of TItemButton;

  { TTimetableForm }

  TTimetableForm = class(TForm, IListener)
    ButtonConflicts: TSpeedButton;
    DataSource: TDataSource;
    DatePickerLeft: TDateTimePicker;
    DatePickerRight: TDateTimePicker;
    GroupBoxPeriods: TGroupBox;
    GroupBoxShown: TGroupBox;
    GroupBoxLabeled: TGroupBox;
    LabelLeft: TLabel;
    LabelRight: TLabel;
    LabelColumns: TLabel;
    LabelRows: TLabel;
    MainMenu: TMainMenu;
    MenuItem1: TMenuItem;
    MenuItemClose: TMenuItem;
    MenuItemSep: TMenuItem;
    MenuItemExport: TMenuItem;
    MenuItemFile: TMenuItem;
    PopupMenuConflicts: TPopupMenu;
    SaveDialog: TSaveDialog;
    ScrollBoxFilters: TScrollBox;
    SidePanel: TPanel;
    RowsCB: TComboBox;
    ColsCB: TComboBox;
    DrawGrid: TDrawGrid;
    SpeedButtonPer1: TSpeedButton;
    SpeedButtonPer2: TSpeedButton;
    SpeedButtonPer3: TSpeedButton;
    SpeedButtonPer4: TSpeedButton;
    SpeedButtonPer5: TSpeedButton;
    SpeedButtonAddFilter: TSpeedButton;
    SpeedButtonApply: TSpeedButton;
    Splitter: TSplitter;
    SQLQuery: TSQLQuery;
    SQLTransaction: TSQLTransaction;
    procedure ButtonConflictsClick(Sender: TObject);
    constructor Create(TheOwner: TComponent);
    procedure DrawGridClick(Sender: TObject);
    procedure DrawGridDblClick(Sender: TObject);
    procedure DrawGridDragDrop(Sender, Source: TObject; X, Y: integer);
    procedure DrawGridDragOver(Sender, Source: TObject; X, Y: integer;
      State: TDragState; var Accept: boolean);
    procedure DrawGridDrawCell(Sender: TObject; aCol, aRow: integer;
      aRect: TRect; aState: TGridDrawState);
    procedure DrawGridMouseMove(Sender: TObject; Shift: TShiftState; X, Y: integer);
    procedure DrawGridStartDrag(Sender: TObject; var DragObject: TDragObject);
    procedure MenuItemCloseClick(Sender: TObject);
    procedure MenuItemExportClick(Sender: TObject);
    procedure PopupItemClick(Sender: TObject);
    procedure SpeedButtonAddFilterClick(Sender: TObject);
    procedure SpeedButtonApplyClick(Sender: TObject);
    procedure InitData(ColID, RowID: integer);
    procedure FillTable();
    procedure FillFixed(TableIndex: integer; Column: boolean);
    procedure DrawFixedCell(aCol, aRow: integer; aRect: TRect);
    procedure DrawItemSidebar(ACol, ARow: integer; Selected: boolean);
    procedure DrawItemButtons(ASidebar: TRect; AItem: integer;
      AButtons: TItemButtons);
    function GetItemHeight(): integer;
    procedure SpeedButtonPerClick(Sender: TObject);
    procedure UpdateCheckboxes();
    procedure UpdateLabeledCheckboxes();
    procedure UpdateRowHeights();
    procedure OnCheckboxToggle(Sender: TObject);
    procedure HandleMessage(Channel, Event, Details: string);

    procedure AddFilter;
    procedure RemoveFilter(Filter: TFilterPanel);
    procedure ChangeFilter(Filter: TFilterPanel);

    procedure CenterOn(col, row: integer);
    procedure RegisterConflicts(t: TConflictType);
  private
    { private declarations }
    Data: TTimetableArray;
    FiltersChanged: boolean;
    draggedItem: integer;
    maxwidths: array of array of integer;
    curCols, curRows: integer;
    Filters: TFPObjectList;

    function IsExpandVisible(aCol, aRow: integer): boolean;
    function GetItemButtons(aCol, aRow, aItem: integer): TItemButtons;

    function GetButtonAt(aCoords: TPoint; aCell: TRect): TItemButton;

    function GetCellByMouse: TPoint;
    function GetItemById(aCol, aRow, aId: integer): integer;
    function GetItemByIdAndDate(aCol, aRow, aId: integer; aDate: TDate): integer;
    function GetItemNumAt(aPoint: TPoint; aCell: TRect): integer;
    function GetItemNumByMouse: integer;

    procedure CreateCard(aCol, aRow: integer);
    procedure CreateView(aCol, aRow: integer);
    procedure DeleteItem(AId: integer);
    procedure ShowPopup(aCol, aRow, aItem: integer);

  public
    { public declarations }
  end;

var
  TimetableForm: TTimetableForm;
  CELL_ITEM_EXPAND_ICON: TPortableNetworkGraphic;
  CELL_ITEM_ADD_ICON: TPortableNetworkGraphic;
  CELL_ITEM_EDIT_ICON: TPortableNetworkGraphic;
  CELL_ITEM_DELETE_ICON: TPortableNetworkGraphic;
  CELL_ITEM_WARNING_ICON: TPortableNetworkGraphic;
  CELL_ITEM_NEWWIN_ICON: TPortableNetworkGraphic;

  const COL_WIDTH: integer = 300;
  const ROW_HEIGHT: integer = 120;
  const CELL_MARGIN: integer = 5;
  const CELL_COLOR: TColor = clWhite;
  const CELL_SELECTED_COLOR: TColor = clSkyBlue;
  const CELL_PEN_COLOR: TColor = clGray;
  const CELL_PEN_SELECTED_COLOR: TColor = clBlack;
  const CELL_SIDE_BAR_RIGHT_OFFSET: integer = 3;
  const CELL_SIDE_BAR_WIDTH: integer = 25;
  const CELL_ITEM_MARGIN: integer = 3;
  const CELL_EXPAND_OFFSET: integer = 1;
  const CELL_ICON_SIZE: integer = 16;
  const CELL_LINES_OFFSET: integer = 20;

  procedure LoadIcons();

implementation

{$R *.lfm}

procedure LoadIcons;
begin
  CELL_ITEM_ADD_ICON := TPortableNetworkGraphic.Create;
  CELL_ITEM_ADD_ICON.LoadFromFile('./icon/add.png');
  CELL_ITEM_EDIT_ICON := TPortableNetworkGraphic.Create;
  CELL_ITEM_EDIT_ICON.LoadFromFile('./icon/edit.png');
  CELL_ITEM_NEWWIN_ICON := TPortableNetworkGraphic.Create;
  CELL_ITEM_NEWWIN_ICON.LoadFromFile('./icon/newwin.png');
  CELL_ITEM_EXPAND_ICON := TPortableNetworkGraphic.Create;
  CELL_ITEM_EXPAND_ICON.LoadFromFile('./icon/expand.png');
  CELL_ITEM_DELETE_ICON := TPortableNetworkGraphic.Create;
  CELL_ITEM_DELETE_ICON.LoadFromFile('./icon/remove.png');
  CELL_ITEM_WARNING_ICON := TPortableNetworkGraphic.Create;
  CELL_ITEM_WARNING_ICON.LoadFromFile('./icon/warn.png');
end;

{ TTimetableForm }

procedure TTimetableForm.FillTable;
var
  i, ii, row, col, recrow, reccol: integer;
  tt: TTableInfo;
  x: TTimetableItem;
  p: TFilterPanel;
  rowname, colname: string;
begin
  FillFixed(curCols, True);
  FillFixed(curRows, False);
  tt := GetTableInfo(TIMETABLENAME);

  SQLTransaction.Commit;
  SQLQuery.Close;

  rowname := tt.GetRelatedActualField(RegisteredTables[curRows].Name).Name;
  colname := tt.GetRelatedActualField(RegisteredTables[curCols].Name).Name;

  SQLQuery.SQL.Text := tt.GetSelectQuery(['rdate', rowname, colname],
    'FullTimetable(:dpl, :dpr)'
  );
  SQLQuery.SQL.Add(GetFilterQuery(tt, Filters));
  SQLQuery.SQL.Add(Format(' ORDER BY t0.%s, ', [rowname]));
  if curRows <> curCols then
      SQLQuery.SQL.Add(Format('t0.%s, ', [colname]));
  SQLQuery.SQL.Add('1, t0.WEEKDAY_ID, t0.LESSON_TIME_ID');
  SQLQuery.Prepare;
  SQLQuery.ParamByName('dpl').AsDateTime := DatePickerLeft.Date;
  SQLQuery.ParamByName('dpr').AsDateTime := DatePickerRight.Date;
  for i := 0 to Filters.Count - 1 do begin
    p := TFilterPanel(Filters.Items[i]);
    if (p.FieldID = -1) or (p.Op = '') or (p.Parameter = '') then continue;
    try
      SQLQuery.ParamByName('prm' + IntToStr(i)).AsString := p.EditValue.Text;
    except
      ShowMessage(Format('Ошибка в фильтре "%s %s %s". Запрос отменен.',
        [tt.DisplayFields[p.FieldID].Caption, p.Op, p.EditValue.Text]));
      exit();
    end;
  end;
  SQLQuery.Open;

  DataSource.DataSet.First;
  row := 1;
  col := 1;
  while not DataSource.DataSet.EOF do begin
    recrow := DataSource.DataSet.Fields[1].AsInteger;
    reccol := DataSource.DataSet.Fields[2].AsInteger;

    while Data[0, row, 0].id <> recrow do
    begin
       row += 1;
       col := 1;
    end;
    while Data[col, 0, 0].id <> reccol do
       col += 1;

    SetLength(Data[col, row], length(Data[col, row]) + 1);
    SetLength(x.info, 0);
    SetLength(x.info, Length(tt.Relations) + 1);

    x.id := DataSource.DataSet.Fields[3].AsInteger;

    for i := 1 to High(tt.DisplayFields) do begin
      ii := tt.GetRelationId(tt.DisplayFields[i].Table.Name);
      if ii = -1 then continue;
      if Length(x.info[ii]) > 0 then
          x.info[ii] += tt.Relations[ii].Joined.Separator +
            DataSource.DataSet.Fields[i + 3].AsString
      else
          x.info[ii] := DataSource.DataSet.Fields[i + 3].AsString;
    end;
    x.info[High(x.info)] := DataSource.DataSet.Fields[0].AsString;
    Data[col, row, High(Data[col, row])] := x;
    DataSource.DataSet.Next;
  end;

  DrawGrid.ColWidths[0] := 300;
  DrawGrid.RowHeights[0] := 60;
  DrawGrid.Invalidate;
end;

procedure TTimetableForm.FillFixed(TableIndex: integer; Column: boolean);
var
  i, j: integer;
  tmp: string;
  prov: TForeignTableProvider;
begin
  with metadata.RegisteredTables[TableIndex] do begin
    prov := dbprovider.Provide(Name);
    prov.Source.DataSet.First;

    i := 1;
    while not prov.Source.DataSet.EOF do begin
      tmp := '';
      for j := 1 to High(DisplayFields) do begin
        if length(tmp) > 0 then
          tmp += ' ';

        if Name = 'Lesson_Times' then begin
          if length(tmp) > 0 then
            tmp += '- ';
          tmp += TimeToStr(prov.Source.DataSet[DisplayFields[j].Name]);
        end
        else begin
          if DisplayFields[j].FieldType = Varchar then
            tmp += prov.Source.DataSet[DisplayFields[j].Name]
          else if DisplayFields[j].FieldType = Numerical then
            tmp += IntToStr(prov.Source.DataSet[DisplayFields[j].Name]);
        end;
      end;

      if Column then begin
        SetLength(Data[i, 0], 1);
        SetLength(Data[i, 0, 0].info, 1);
        Data[i, 0, 0].id := prov.Source.DataSet['id'];
        Data[i, 0, 0].info[0] := tmp;
      end
      else begin
        SetLength(Data[0, i], 1);
        SetLength(Data[0, i, 0].info, 1);
        Data[0, i, 0].id := prov.Source.DataSet['id'];
        Data[0, i, 0].info[0] := tmp;
      end;

      prov.Source.DataSet.Next;
      Inc(i);
    end;
  end;
end;

procedure TTimetableForm.SpeedButtonAddFilterClick(Sender: TObject);
begin
  AddFilter;
  FiltersChanged := True;
end;

procedure TTimetableForm.SpeedButtonApplyClick(Sender: TObject);
var
  s: string;
begin
  curCols := ColsCB.ItemIndex;
  curRows := RowsCB.ItemIndex;
  if (curRows = -1) or (curCols = -1) then begin
    s := 'Не указан источник для ';
    if curRows = -1 then
      s += 'строк';
    if curCols = -1 then begin
      if length(s) > 0 then
        s += ' и ';
      s += 'столбцов';
    end;
    Application.MessageBox(@(s[1]), 'Ошибка', MB_ICONSTOP);
    exit();
  end;
  InitData(curCols, curRows);
  UpdateCheckboxes();
  FiltersChanged := False;
end;

procedure TTimetableForm.InitData(ColID, RowID: integer);
var
  cols, rows, i: integer;
begin
  cols := Provide(RegisteredTables[ColID].Name).Count + 1;
  rows := Provide(RegisteredTables[RowID].Name).Count + 1;

  DrawGrid.Enabled := True;
  MenuItemExport.Enabled := True;

  with DrawGrid do begin
    ColCount := cols;
    RowCount := rows;
    FixedCols := 1;
    FixedRows := 1;
  end;

  SetLength(Data, 0);
  SetLength(Data, cols);
  for i := 0 to cols - 1 do begin
    SetLength(Data[i], rows);
    DrawGrid.ColWidths[i] := COL_WIDTH;
  end;

  SetLength(maxwidths, cols, rows);

  for i := 0 to rows - 1 do begin
    DrawGrid.RowHeights[i] := max(DrawGrid.RowHeights[i], ROW_HEIGHT);
  end;

  FillTable();
  with eventnotify.GetHub(TIMETABLENAME + 'card') do begin
    Send('SetLock',
      RegisteredTables[ColID].Name + ';' + RegisteredTables[RowID].Name);
  end;
  conflicts.ConflictManager.GetConflicts(@self.RegisterConflicts);
end;

procedure TTimetableForm.DrawFixedCell(aCol, aRow: integer; aRect: TRect);
var
  style: TTextStyle;
  r: TRect;
begin
  style.EndEllipsis := False;
  style.SingleLine := False;
  style.Wordbreak := True;
  style.Alignment := taLeftJustify;

  r := aRect;
  r.Left += CELL_MARGIN;
  r.Top += CELL_MARGIN;
  r.Right -= CELL_MARGIN;
  r.Bottom -= CELL_MARGIN;

  if aRow > 0 then begin
    if (Length(Data[0]) = 0) then
      exit;
    DrawGrid.Canvas.TextRect(r, r.Left, r.Top, Data[0, aRow][0].info[0], style);
  end
  else if aCol > 0 then begin
    if (Length(Data) = 0) then
      exit;
    DrawGrid.Canvas.TextRect(r, r.Left, r.Top, Data[aCol, 0][0].info[0], style);
  end;
end;

procedure TTimetableForm.DrawItemSidebar(ACol, ARow: integer; Selected: boolean);
var
  c: TPoint;
  aRect, x: TRect;
  i, basey: integer;
  buttons: TItemButtons;
  button : TItemButton;
  ic: TPortableNetworkGraphic;
  cellactive: boolean;
begin
  aRect := DrawGrid.CellRect(ACol, ARow);

  if Selected then
    DrawGrid.Canvas.Pen.Color := CELL_PEN_SELECTED_COLOR
  else
    DrawGrid.Canvas.Pen.Color := CELL_PEN_COLOR;

  // Separate side
  x.Top := aRect.Top;
  x.Left := aRect.Right - CELL_SIDE_BAR_WIDTH;
  x.Bottom := aRect.Bottom;
  x.Right := aRect.Right;

  with DrawGrid.Canvas do begin
    if Selected then
      Brush.Color := CELL_SELECTED_COLOR
    else
      Brush.Color := CELL_COLOR;
    Rectangle(x);
    Line(x.Left, x.Top, x.Left, x.Bottom);
  end;
  c := GetCellByMouse;

  if DrawGrid.Dragging and (draggedItem <> -1) then begin
    if (c.x = ACol) and (c.y = ARow) then
      DrawGrid.Canvas.Rectangle(aRect.Left, aRect.Top, aRect.Right,
        aRect.Top + 2);
  end;

  x.Top := aRect.Top;
  x.Left := aRect.Right - CELL_EXPAND_OFFSET - CELL_ICON_SIZE - CELL_ITEM_MARGIN;
  x.Bottom := aRect.Bottom;
  x.Right := aRect.Right;

  aRect.Right -= CELL_SIDE_BAR_RIGHT_OFFSET;

  cellactive := Selected or ((c.x = aCol) and (c.y = aRow));
  for i := 0 to High(Data[aCol, aRow]) do begin
    if cellactive then
      buttons := GetItemButtons(aCol, aRow, i)
    else if Length(Data[aCol, aRow, i].conflicts) > 0 then begin
      if i = 0 then buttons := [NONE, CONFLICT]
      else buttons := [CONFLICT];
    end
    else
      break;
    DrawItemButtons(x, i, buttons);
  end;
  if Length(Data[aCol, aRow]) = 0 then
    DrawItemButtons(x, 0, GetItemButtons(aCol, aRow, 0));

  // Draw 'Expand' button
  if IsExpandVisible(aCol, aRow) then begin
    x.Top := aRect.Bottom - CELL_EXPAND_OFFSET - CELL_ICON_SIZE;
    x.Left := aRect.Right - CELL_EXPAND_OFFSET - CELL_ICON_SIZE;
    x.Bottom := x.Top + CELL_ICON_SIZE;
    x.Right := x.Left + CELL_ICON_SIZE;

    DrawGrid.Canvas.Draw(x.Left, x.Top, CELL_ITEM_EXPAND_ICON);
  end;

end;

procedure TTimetableForm.DrawItemButtons(ASidebar: TRect; AItem: integer;
  AButtons: TItemButtons);
var
  button: TItemButton;
  basey: integer;
  ic: TPortableNetworkGraphic;
begin
  basey := GetItemHeight() * AItem + CELL_ITEM_MARGIN;
  for button in AButtons do begin
    case button of
      NEWWIN: ic := CELL_ITEM_NEWWIN_ICON;
      ADD: ic := CELL_ITEM_ADD_ICON;
      EDIT: ic := CELL_ITEM_EDIT_ICON;
      DELETE: ic := CELL_ITEM_DELETE_ICON;
      CONFLICT: ic := CELL_ITEM_WARNING_ICON;
      NONE: begin
        basey += CELL_ICON_SIZE + CELL_ITEM_MARGIN;
        continue;
      end;
    end;
    DrawGrid.Canvas.Draw(ASidebar.Left, ASidebar.Top + basey, ic);
    basey += CELL_ICON_SIZE + CELL_ITEM_MARGIN;
  end;
end;

function TTimetableForm.GetItemHeight: integer;
var
  i: integer;
begin
  Result := 0;
  for i := 0 to GroupBoxShown.ComponentCount - 1 do begin
    if TCheckbox(GroupBoxShown.Components[i]).Checked then
      Result += CELL_LINES_OFFSET;
  end;
end;

procedure TTimetableForm.SpeedButtonPerClick(Sender: TObject);
begin
  case TComponent(Sender).Tag of
    1: DatePickerRight.Date := DatePickerLeft.Date;
    2: DatePickerRight.Date := IncDay(DatePickerLeft.Date, 6);
    3: DatePickerRight.Date := IncMonth(DatePickerLeft.Date);
    4: DatePickerRight.Date := IncMonth(DatePickerLeft.Date, 4);
    5: DatePickerRight.Date := IncMonth(DatePickerLeft.Date, 12);
  end;
end;

procedure TTimetableForm.UpdateCheckboxes;
var
  i: integer;
  tt: TTableInfo;
begin
  tt := metadata.GetTableInfo(TIMETABLENAME);
  for i := 0 to High(tt.Relations) do begin
    if (tt.Relations[i].Joined.Caption = ColsCB.Items[curCols]) or
      (tt.Relations[i].Joined.Caption = RowsCB.Items[curRows]) then begin
      TCheckbox(GroupBoxShown.Controls[i]).Checked := False;
    end
    else
      TCheckbox(GroupBoxShown.Controls[i]).Checked := True;
  end;
  UpdateLabeledCheckboxes();
end;

procedure TTimetableForm.UpdateLabeledCheckboxes;
var
  i: integer;
begin
  for i := 0 to GroupBoxLabeled.ControlCount - 1 do begin
    GroupBoxLabeled.Controls[i].Enabled :=
      TCheckBox(GroupBoxShown.Controls[i]).Checked;
  end;
end;

procedure TTimetableForm.UpdateRowHeights;
var
  i: integer;
begin
  for i := 1 to DrawGrid.RowCount - 1 do begin
    DrawGrid.RowHeights[i] := max(GetItemHeight(), DrawGrid.RowHeights[i]);
  end;
end;

procedure TTimetableForm.OnCheckboxToggle(Sender: TObject);
begin
  DrawGrid.Invalidate;
  UpdateLabeledCheckboxes();
end;

procedure TTimetableForm.HandleMessage(Channel, Event, Details: string);
begin
  if Event <> 'CardOpened' then begin
    if Length(Data) = 0 then
      exit();

    with eventnotify.GetHub(Channel + 'card' + Details) do begin
      Send('SetLock',
        RegisteredTables[curCols].Name + ';' + RegisteredTables[curRows].Name);
    end;
  end
  else if Event = 'Update' then begin
    InitData(curCols, curRows);
  end;
end;

procedure TTimetableForm.AddFilter;
var
  fp: TFilterPanel;
begin
  ScrollBoxFilters.Visible := True;
  SpeedButtonAddFilter.Visible := False;
  fp := TFilterPanel.Create(self, GetTableInfo(TIMETABLENAME), @self.AddFilter,
    @self.RemoveFilter, @self.ChangeFilter);
  Filters.Add(fp);
  ScrollBoxFilters.InsertControl(fp);
end;

procedure TTimetableForm.RemoveFilter(Filter: TFilterPanel);
begin
  ScrollBoxFilters.RemoveControl(Filter);
  Filters.Delete(Filters.IndexOf(Filter));
  if (Filters.Count > 0) then begin
    (Filters.Last as TFilterPanel).SpeedAddFilter.Visible := True;
  end
  else begin
    ScrollBoxFilters.Visible := False;
  end;
  SpeedButtonAddFilter.Visible := True;
end;

procedure TTimetableForm.ChangeFilter(Filter: TFilterPanel);
begin
  FiltersChanged := True;
end;

function TTimetableForm.IsExpandVisible(aCol, aRow: integer): boolean;
begin
  exit((GetItemHeight() * Length(Data[aCol, aRow]) > DrawGrid.RowHeights[aRow]) or
    (maxwidths[aCol, aRow] > DrawGrid.ColWidths[aCol] - CELL_SIDE_BAR_WIDTH));
end;

function TTimetableForm.GetItemButtons(aCol, aRow, aItem: integer): TItemButtons;
var
  i: TItemButton;
  j, itempos: integer;
begin
  if Length(Data[aCol, aRow]) = 0 then exit([TItemButton.ADD]);
  if aItem = 0 then
    i := NEWWIN
  else
    i := CONFLICT;

  j := 1;
  Result := [];
  for i := i to High(TItemButton) do begin
    itempos := (GetItemHeight * aItem) + j * (CELL_ICON_SIZE + CELL_ITEM_MARGIN);
    if (itempos < DrawGrid.RowHeights[aRow] - CELL_ICON_SIZE - CELL_EXPAND_OFFSET)
    and (itempos < GetItemHeight * (aItem + 1))
    then begin
      Result += [i];
    end;
    j += 1;
  end;
  if Length(Data[aCol, aRow, aItem].conflicts) = 0 then
    Result -= [CONFLICT];
end;

function TTimetableForm.GetButtonAt(aCoords: TPoint; aCell: TRect): TItemButton;
var
  num, index, i: integer;
  b: TItemButton;
  buttons: TItemButtons;
  c: TPoint;
begin
  // TODO WARNING ICON
  if (aCoords.x < aCell.Right - CELL_SIDE_BAR_WIDTH) or
    (aCoords.x > aCell.Right - CELL_EXPAND_OFFSET - CELL_ITEM_MARGIN) then
    exit(NONE);

  // Get item id
  num := GetItemNumAt(aCoords, aCell);
  c := DrawGrid.MouseToCell(aCoords);
  if num = -1 then
    exit(NONE);
  // Get top offset

  index := aCoords.y - (aCell.Top + num * GetItemHeight() + CELL_ITEM_MARGIN);
  index := index div (CELL_ICON_SIZE + CELL_ITEM_MARGIN);

  buttons := GetItemButtons(c.x, c.y, num);
  i := 0;
  for b in buttons do begin
    if i = index then exit(b);
    i += 1;
  end;
  exit(NONE);
end;

function TTimetableForm.GetCellByMouse: TPoint;
begin
  Result := DrawGrid.MouseToCell(ScreenToClient(Mouse.CursorPos));
end;

function TTimetableForm.GetItemById(aCol, aRow, aId: integer): integer;
var
  i: integer;
begin
  for i := 0 to High(Data[aCol, aRow]) do begin
    if aId = Data[aCol, aRow, i].id then exit(i);
  end;
  exit(-1);
end;

function TTimetableForm.GetItemByIdAndDate(aCol, aRow, aId: integer;
  aDate: TDate): integer;
var
  i: integer;
  d: string;
begin
  d := DateToStr(aDate);
  for i := 0 to High(Data[aCol, aRow]) do begin
    if (aId = Data[aCol, aRow, i].id) and (d = Data[aCol, aRow, i].info[7]) then
      exit(i);
  end;
  exit(-1);
end;

function TTimetableForm.GetItemNumAt(aPoint: TPoint; aCell: TRect): integer;
begin
  exit((aPoint.y - aCell.Top) div GetItemHeight());
end;

function TTimetableForm.GetItemNumByMouse: integer;
var
  cell: TPoint;
begin
  cell := GetCellByMouse;
  exit(GetItemNumAt(ScreenToClient(Mouse.CursorPos),
    DrawGrid.CellRect(cell.x, cell.y)));
end;

procedure TTimetableForm.CreateCard(aCol, aRow: integer);
var
  card: TEditForm;
  fname: string;
  tt: TTableInfo;
begin
  fCard.EditItem(self, TIMETABLENAME, -1);
  card := fCard.GetCard(TIMETABLENAME, -1);
  tt := metadata.GetTableInfo(TIMETABLENAME);

  fname := RegisteredTables[curCols].Name;
  fname := tt.GetRelatedDisplayField(fname).Name;
  if Data[aCol, 0, 0].id <> -1 then
    card.SetIntValue(RegisteredTables[curCols].Name, fname, Data[aCol, 0, 0].id);

  fname := RegisteredTables[curRows].Name;
  fname := tt.GetRelatedDisplayField(fname).Name;
  if Data[0, aRow, 0].id <> -1 then
    card.SetIntValue(RegisteredTables[curRows].Name, fname, Data[0, aRow, 0].id);
  card.SetStrValue(TIMETABLENAME, 'start_date', DateToStr(DatePickerLeft.Date));
  card.SetStrValue(TIMETABLENAME, 'end_date', DateToStr(DatePickerRight.Date));
  with eventnotify.GetHub(TIMETABLENAME + 'card-1') do begin
    Send('SetLock',
      RegisteredTables[curCols].Name + ';' + RegisteredTables[curRows].Name);
  end;
end;

procedure TTimetableForm.CreateView(aCol, aRow: integer);
var
  v: TViewForm;
  fname, sep, filter: string;
  tt: TTableInfo;
  fi: TFieldInfo;
  ci: integer;
begin
  v := TViewForm.Create(self, metadata.GetTableId(TIMETABLENAME));
  tt := metadata.GetTableInfo(TIMETABLENAME);

  fi := tt.GetRelatedDisplayField(RegisteredTables[curCols].Name);
  ci := tt.GetConcatInfoId(fi);
  if ci <> -1 then
    filter := SplitString(Data[aCol, 0, 0].info[0],
      tt.Concats[ci].Separators[0][1])[0]
  else
    filter := Data[aCol, 0, 0].info[0];
  v.AddFilter(fi.Caption, VarcharOps[0], filter);
  fi := tt.GetRelatedDisplayField(RegisteredTables[curRows].Name);
  ci := tt.GetConcatInfoId(fi);
  if ci <> -1 then
    filter := SplitString(Data[0, aRow, 0].info[0],
      tt.Concats[ci].Separators[0][1])[0]
  else
    filter := Data[0, aRow, 0].info[0];
  if Lowercase(fi.Table.Name) = 'lesson_times' then
    filter := SplitString(filter, ' ')[0];
  v.AddFilter(fi.Caption, VarcharOps[0], filter);
  v.SpeedButtonApplyClick(v.SpeedButtonApply);

  v.Show;
end;

procedure TTimetableForm.DeleteItem(AId: integer);
var
  ans: integer;
  editor: TEditForm;
begin
  ans := Application.MessageBox('Вы действительно хотите удалить эту запись?',
    'Удаление', MB_YESNO + MB_ICONQUESTION);
  if (ans = idYes) then begin
    editor := fCard.GetCard(TIMETABLENAME, AId);
    if editor <> nil then
      editor.ForceClose();
    try
      dbprovider.Provide(TIMETABLENAME).DeleteItem(AId);
      ShowMessage('Запись удалена');
    except
      on e: Exception do begin
        ShowMessage('Ошибка при удалении записи: ' + #13#10 + e.Message);
      end;
    end;
  end;
end;

procedure TTimetableForm.ShowPopup(aCol, aRow, aItem: integer);
var
  i: integer;
  mi: TMenuItem;
  c: TConflictType;
begin
  PopupMenuConflicts.Items.Clear;
  for i := 0 to High(Data[aCol, aRow, aItem].conflicts) do begin
    c := TConflictType(Data[aCol, aRow, aItem].conflicts[i]);
    mi := TMenuItem.Create(PopupMenuConflicts);
    mi.Caption := c.Description;
    mi.Tag := i;
    mi.OnClick:=@self.PopupItemClick;
    PopupMenuConflicts.Items.Add(mi);
  end;
  PopupMenuConflicts.PopUp;
end;

constructor TTimetableForm.Create(TheOwner: TComponent);
var
  i: integer;
  tt: TTableInfo;
  tcb: TCheckbox;
begin
  inherited Create(TheOwner);

  LoadIcons();
  draggedItem := -1;
  Filters := TFPObjectList.Create();
  FiltersChanged := False;
  DatePickerRight.Date := IncDay(DatePickerLeft.Date, 6);

  eventnotify.GetHub(TIMETABLENAME).RegisterListener(self);
  eventnotify.GetHub(TIMETABLENAME + 'card').RegisterListener(self);

  for i := 0 to High(metadata.RegisteredTables) do begin
    if metadata.RegisteredTables[i].Name <> TIMETABLENAME then begin
      RowsCB.Items.Add(metadata.RegisteredTables[i].Caption);
      ColsCB.Items.Add(metadata.RegisteredTables[i].Caption);
    end;
  end;

  tt := metadata.GetTableInfo(TIMETABLENAME);
  for i := 0 to High(tt.Relations) do begin
    tcb := TCheckBox.Create(GroupBoxShown);
    tcb.Tag := i;
    tcb.Checked := True;
    tcb.Caption := tt.Relations[i].Joined.RecordName;
    tcb.OnChange := @OnCheckboxToggle;
    GroupBoxShown.InsertControl(tcb);

    tcb := TCheckBox.Create(GroupBoxLabeled);
    tcb.Tag := i;
    tcb.Checked := True;
    tcb.Caption := tt.Relations[i].Joined.RecordName;
    tcb.OnChange := @OnCheckboxToggle;
    GroupBoxLabeled.InsertControl(tcb);
  end;

  tcb := TCheckBox.Create(GroupBoxShown);
  tcb.Tag := i + 1;
  tcb.Checked := True;
  tcb.Caption := 'Дата проведения';
  tcb.OnChange := @OnCheckboxToggle;
  GroupBoxShown.InsertControl(tcb);

  tcb := TCheckBox.Create(GroupBoxLabeled);
  tcb.Tag := i + 1;
  tcb.Checked := True;
  tcb.Caption := 'Дата проведения';
  tcb.OnChange := @OnCheckboxToggle;
  GroupBoxLabeled.InsertControl(tcb);

  SQLTransaction.DataBase := dbprovider.DbData.DBConnection;
  SQLQuery.Transaction := SQLTransaction;
  DataSource.DataSet := SQLQuery;
end;

procedure TTimetableForm.ButtonConflictsClick(Sender: TObject);
var
  v: TConflictViewForm;
begin
  v := TConflictViewForm.Create(self, addr(curRows), addr(curCols), @CenterOn);
  v.DatePickerLeft.Date := self.DatePickerLeft.Date;
  v.DatePickerRight.Date := self.DatePickerRight.Date;
  v.SpeedButtonApplyClick(v.SpeedButtonApply);
  v.Show;
end;

procedure TTimetableForm.DrawGridClick(Sender: TObject);
var
  p, c: TPoint;
  r: TRect;
  aCol, aRow: integer;
  itemnum: integer;
  button: TItemButton;
begin
  p := ScreenToClient(Mouse.CursorPos);

  c := GetCellByMouse;
  aCol := c.x;
  aRow := c.y;

  if (aCol = 0) or (aRow = 0) then
    exit();
  r := DrawGrid.CellRect(aCol, aRow);

  if p.x < r.Right - CELL_SIDE_BAR_WIDTH then
    exit();

  if IsExpandVisible(aCol, aRow) and (p.y >= r.Bottom - CELL_ICON_SIZE -
    CELL_EXPAND_OFFSET) then begin
    with DrawGrid do begin
      if RowHeights[aRow] < GetItemHeight() * Length(Data[aCol, aRow]) then
        RowHeights[aRow] := GetItemHeight() * Length(Data[aCol, aRow]);

      if ColWidths[aCol] < maxwidths[aCol, aRow] + CELL_SIDE_BAR_WIDTH then
        ColWidths[aCol] := maxwidths[aCol, aRow] + CELL_SIDE_BAR_WIDTH;
    end;
  end
  else begin
    itemnum := GetItemNumAt(p, r);
    button := GetButtonAt(p, r);
    if (itemnum > High(Data[aCol, aRow])) and not
      ((Length(Data[aCol, aRow]) = 0) and (button = ADD))
    then
      exit();
    case button of
      NEWWIN: CreateView(aCol, aRow);
      ADD: CreateCard(aCol, aRow);
      EDIT: begin
        fCard.EditItem(self, TIMETABLENAME, Data[aCol, aRow, itemnum].id);
        fCard.GetCard(TIMETABLENAME, Data[aCol, aRow, itemnum].id).HasChanged := False;
      end;
      DELETE: DeleteItem(Data[aCol, aRow, itemnum].id);
      CONFLICT: ShowPopup(aCol, aRow, itemnum);
    end;
  end;
end;

procedure TTimetableForm.DrawGridDblClick(Sender: TObject);
var
  p: TPoint;
  item: integer;
  buttons: TItemButtons;
begin
  p := GetCellByMouse;
  item := GetItemNumByMouse;
  if (p.x < 1) or (p.y < 1) or (item = -1) or (item > High(Data[p.x, p.y])) then
    exit();
  buttons := GetItemButtons(p.x, p.y, item);
  fCard.EditItem(self, TIMETABLENAME, Data[curCols, curRows, item].id);
end;

procedure TTimetableForm.DrawGridDragDrop(Sender, Source: TObject; X, Y: integer);
var
  c: TPoint;
  tt: TTableInfo;
begin
  if draggedItem = -1 then
    exit();
  c := GetCellByMouse;
  if (c.x = 0) or (c.y = 0) then
    exit();

  try
    tt := GetTableInfo(TIMETABLENAME);
    SQLTransaction.Commit;
    SQLQuery.Close;
    SQLQuery.SQL.Text := Format(
      'UPDATE %s SET %s = :prm0, %s = :prm1 WHERE id = :prmid',
      [TIMETABLENAME,
        tt.GetRelatedActualField(RegisteredTables[curCols].Name).Name,
        tt.GetRelatedActualField(RegisteredTables[curRows].Name).Name
      ]
    );
    SQLQuery.Prepare;
    SQLQuery.Params.ParamByName('prm0').AsInteger := Data[c.x, 0, 0].id;
    SQLQuery.Params.ParamByName('prm1').AsInteger := Data[0, c.y, 0].id;
    SQLQuery.Params.ParamByName('prmid').AsInteger := draggedItem;
    SQLQuery.ExecSQL;
    SQLTransaction.Commit;
    if ProviderExists(TIMETABLENAME) then
      eventnotify.GetHub(TIMETABLENAME).Send('UpdateProviders', '')
    else
      eventnotify.GetHub(TIMETABLENAME).Send('Update', '');
  except
    on E: Exception do begin
      ShowMessage('Ошибка при обновлении базы данных: ' + e.Message);
    end;
  end;

  draggedItem := -1;
end;

procedure TTimetableForm.DrawGridDragOver(Sender, Source: TObject;
  X, Y: integer; State: TDragState; var Accept: boolean);
begin
  // So it will accept everything
end;

procedure TTimetableForm.DrawGridDrawCell(Sender: TObject; aCol, aRow: integer;
  aRect: TRect; aState: TGridDrawState);
var
  r: TRect;
  x: TTimetableItem;
  i, offset, j: integer;
  s: string;
  tt: TTableInfo;
begin
  offset := 0;
  if (aCol = 0) or (aRow = 0) then begin
    DrawFixedCell(aCol, aRow, aRect);
  end
  else begin
    if gdSelected in aState then
      DrawGrid.Canvas.Brush.Color := CELL_SELECTED_COLOR
    else
      DrawGrid.Canvas.Brush.Color := CELL_COLOR;

    DrawGrid.Canvas.Rectangle(aRect);

    r := aRect;
    r.Right := r.Right - CELL_SIDE_BAR_WIDTH;

    DrawItemSidebar(ACol, ARow, gdSelected in aState);
    tt := metadata.GetTableInfo(TIMETABLENAME);
    for i := 0 to High(Data[aCol, aRow]) do begin
      x := Data[aCol, aRow, i];
      if Length(x.info) > 0 then begin
        for j := 0 to High(x.info) do begin
          if TCheckBox(GroupBoxShown.Controls[j]).Checked = False then
            Continue;

          s := x.info[j];
          if TCheckBox(GroupBoxLabeled.Controls[j]).Checked then begin
            s := TCheckBox(GroupBoxLabeled.Controls[j]).Caption + ': ' + s;
          end;

          DrawGrid.Canvas.TextRect(r, r.Left, r.Top + offset, s);
          maxwidths[aCol, aRow] :=
            max(DrawGrid.Canvas.TextExtent(s).cx, maxwidths[aCol, aRow]);
          offset += CELL_LINES_OFFSET;
        end;

        DrawGrid.Canvas.Line(aRect.Left, aRect.Top + offset,
          aRect.Right, aRect.Top + offset);
      end;
    end;
  end;
end;

procedure TTimetableForm.DrawGridMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: integer);
begin
  DrawGrid.Invalidate;
end;

procedure TTimetableForm.DrawGridStartDrag(Sender: TObject;
  var DragObject: TDragObject);
var
  c: TPoint;
  item: integer;
begin
  c := GetCellByMouse;
  if (c.x = 0) or (c.y = 0) then
    exit();
  item := GetItemNumByMouse;
  if (item > High(Data[c.x, c.y])) then
    exit();
  draggedItem := Data[c.x, c.y, item].id;
end;

procedure TTimetableForm.MenuItemCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TTimetableForm.MenuItemExportClick(Sender: TObject);
var
  f: array of TFilterPanel;
  i: integer;
begin
  if SaveDialog.Execute then begin
    if FiltersChanged then SpeedButtonApply.Click();
    SetLength(f, Filters.Count);
    for i := 0 to Filters.Count - 1 do begin
      f[i] := TFilterPanel(Filters.Items[i]);
    end;
    ExportTable(self, 0, @Data, [], f, SaveDialog.FileName);
  end;
end;

procedure TTimetableForm.PopupItemClick(Sender: TObject);
var
  i, item, sendertag: integer;
  p: TPoint;
  v: TConflictViewForm;
begin
  p := DrawGrid.Selection.TopLeft;
  item := GetItemNumByMouse;
  sendertag := TComponent(Sender).Tag;
  v := TConflictViewForm.Create(self, addr(curRows), addr(curCols), @CenterOn);
  v.DatePickerLeft.Date := self.DatePickerLeft.Date;
  v.DatePickerRight.Date := self.DatePickerRight.Date;
  v.SpeedButtonApplyClick(v.SpeedButtonApply);
  v.NavigateTo(
    TConflictType(Data[p.x, p.y, item].conflicts[sendertag]).Description,
  p.x, p.y);
  v.Show;
end;

procedure TTimetableForm.CenterOn(col, row: integer);
begin
  DrawGrid.Col := col;
  DrawGrid.Row := row;
  try
    DrawGrid.SetFocus;
    self.BringToFront;
  except
    // Nothing
  end;
end;

procedure TTimetableForm.RegisterConflicts(t: TConflictType);
var
  d: TConflictRecords;
  rowname, colname: string;
  i, row, col, item: integer;
  tt: TTableInfo;
begin
  tt := GetTableInfo(TIMETABLENAME);
  colname := tt.GetRelatedActualField(RegisteredTables[curCols].Name).Name;
  rowname := tt.GetRelatedActualField(RegisteredTables[curRows].Name).Name;
  d := t.GetConflictRecords(rowname, colname,
    DatePickerLeft.Date, DatePickerRight.Date);
  row := 1;
  col := 1;
  for i := 0 to High(d) do begin
    while Data[0, row, 0].id <> d[i].row do begin
      row += 1;
      col := 1;
    end;
    while Data[col, 0, 0].id <> d[i].col do
      col += 1;

    item := GetItemByIdAndDate(col, row, d[i].id, d[i].date);
    if item = -1 then continue;

    with Data[col, row, item] do begin
      SetLength(conflicts, Length(conflicts) + 1);
      conflicts[High(conflicts)] := t;
    end;
  end;
  DrawGrid.Invalidate;
end;

initialization
  LoadIcons();
end.
