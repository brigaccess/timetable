unit eventnotify;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, contnrs, fgl;

type
  IListener = Interface ['{C6A51D0A-73A7-442F-BD39-137A18F48F52}']
    procedure HandleMessage(Channel, Event, Details: String);
  end;

  { TEventHub }

  TEventHub = class(TObject)
    public
      Name: String;
      Handlers: TInterfaceList;
      constructor Create(AName: string);
      procedure Send(Event: String; Details: String);
      procedure RegisterListener(AListener: IListener);
  end;

  var
    Hubs: array of TEventHub;
    function GetHub(TableName: String): TEventHub;

implementation

function GetHub(TableName: String): TEventHub;
var
  i: integer;
begin
  for i := 0 to High(Hubs) do begin
    if (Hubs[i].Name = TableName) then exit(Hubs[i]);
  end;
  // If hub wasn't found, create one
  SetLength(Hubs, Length(Hubs) + 1);
  Hubs[High(Hubs)] := TEventHub.Create(TableName);
  exit(Hubs[High(Hubs)]);
end;

{ TEventHub }

constructor TEventHub.Create(AName: string);
begin
  Handlers := TInterfaceList.Create;
  Name := AName;
end;

procedure TEventHub.Send(Event: String; Details: String);
var
  i: integer;
begin
  for i := 0 to Handlers.Count - 1 do begin
    (Handlers[i] as IListener).HandleMessage(self.Name, Event, Details)
  end;
end;

procedure TEventHub.RegisterListener(AListener: IListener);
begin
  if Handlers.IndexOf(AListener) = -1 then
    Handlers.Add(AListener)
end;

end.

