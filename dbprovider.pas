unit dbprovider;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, IBConnection, FileUtil, sqldb, db, eventnotify, metadata;

type

  { TDbData }

  TDbData = class(TDataModule)
    DBConnection: TIBConnection;
  private
    { private declarations }
  public
    { public declarations }
  end;

  { TForeignTableProvider }

  TForeignTableProvider = class(TInterfacedObject, IListener)
    Name: string;
    Transaction: TSQLTransaction;
    Query: TSQLQuery;
    Source: TDataSource;
    Count: integer;
    constructor Create(Tablename: string);
    function GetIdByField(AFieldname, AFieldcontent: string): integer;
    procedure DeleteItem(AId: integer);
    procedure HandleMessage(Channel, Event, Details: string);
    private
      function GetItems(): integer;
  end;


var
  DbData: TDbData;
  Providers: array of TForeignTableProvider;
  function Provide(Tablename: string): TForeignTableProvider;
  function ProviderExists(Tablename: string): boolean;

implementation

function Provide(Tablename: string): TForeignTableProvider;
var
  i: integer;
  tft: TForeignTableProvider;
begin
  for i := 0 to High(Providers) do begin
    if Providers[i].Name = Tablename then exit(Providers[i]);
  end;
  SetLength(Providers, Length(Providers) + 1);
  tft := TForeignTableProvider.Create(Tablename);
  eventnotify.GetHub(Tablename).RegisterListener(tft);
  tft.HandleMessage(Tablename, 'UpdateProviders', '');
  Providers[High(Providers)] := tft;
  Result := tft;
end;

function ProviderExists(Tablename: string): boolean;
var
  i: integer;
begin
  for i := 0 to High(Providers) do begin
    if Lowercase(Providers[i].Name) = Lowercase(Tablename) then exit(True);
  end;
  exit(False);
end;

{$R *.lfm}

{ TForeignTableProvider }

constructor TForeignTableProvider.Create(Tablename: string);
begin
  inherited Create;
  Name := Tablename;

  Transaction := TSQLTransaction.Create(dbprovider.DbData);
  Transaction.DataBase := dbprovider.DbData.DBConnection;
  Query := TSQLQuery.Create(dbprovider.DbData);
  Query.Transaction := Transaction;
  Source := TDataSource.Create(dbprovider.DbData);
  Source.DataSet := Query;
end;

function TForeignTableProvider.GetIdByField(AFieldname, AFieldcontent: string):
  integer;
begin
  Source.DataSet.First;
  while not Source.DataSet.EOF do begin
    if Source.DataSet[AFieldname] = AFieldcontent then
      exit(Source.DataSet['id']);
    Source.DataSet.Next;
  end;
  exit(-1);
end;

procedure TForeignTableProvider.DeleteItem(AId: integer);
begin
  Transaction.Commit;
  Query.Close;
  Query.SQL.Text := 'DELETE FROM ' + Name + ' WHERE id=:prmid;';
  Query.ParamByName('prmid').AsInteger := AId;
  Query.ExecSQL;
  Transaction.Commit;
  HandleMessage(Name, 'UpdateProviders', '');
end;

function TForeignTableProvider.GetItems: integer;
begin
  Transaction.Commit;
  Query.Close;
  Query.SQL.Clear;
  Query.SQL.Add('SELECT count(*) as c FROM ' + Name + ';');
  Query.Open;
  Result := Query['c'];
  Transaction.Commit;
  Query.Close;
  Query.SQL.Clear;
  Query.SQL.Add('SELECT * FROM ' + Name + ';');
  Query.Open;
end;

procedure TForeignTableProvider.HandleMessage(Channel, Event, Details: string);
begin
  if (Event = 'UpdateProviders') then begin
    Count := GetItems();
    eventnotify.GetHub(Name).Send('Update', '');
  end;
end;


end.

