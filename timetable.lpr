program timetable;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, fMain, fView, dbprovider, metadata, fCard, eventnotify, ftimetable,
  conflicts, exportmanager;

{$R *.res}

begin
  RequireDerivedFormResource := True;
  Application.Initialize;
  Application.CreateForm(TDbData, DbData);
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TConflictManager, ConflictManager);
  Application.Run;
end.

